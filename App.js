import React, { Fragment, Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import RootNavigator from './src/navigation/_RootNavigator'


class App extends Component {

  render() {
    return (
      <RootNavigator />
    );
  }

};





export default App;
