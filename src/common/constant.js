import {
  Dimensions,
  Platform
} from 'react-native';

const screenWidth = Dimensions.get('window').width
const screenHeight = Dimensions.get('window').height
export const iPhoneX = (Platform.OS === "ios" && Dimensions.get("window").height >= 812 && Dimensions.get("window").width >= 375);

export const USERDATA = 'USERDATA';
export const ISLOGIN = 'ISLOGIN';
export const LOCALUSER = 'LOCALUSER';

export const FONT_10 = screenHeight * 0.0162;
export const FONT_12 = screenHeight * 0.0194;
export const FONT_14 = screenHeight * 0.0227;
export const FONT_16 = screenHeight * 0.0259;
export const FONT_20 = screenHeight * 0.0324;
export const FONT_24 = screenHeight * 0.0389;

export const FONTSIZE = {
  FONT_10: screenHeight * 0.0162,
  FONT_12: screenHeight * 0.0194,
  FONT_14: screenHeight * 0.0227,
  FONT_18: screenHeight * 0.0292,
  FONT_16: screenHeight * 0.0259,
  FONT_20: screenHeight * 0.0324,
  FONT_24: screenHeight * 0.0389,
}

export const FONT_WEIGHT = {
  FONT_500: "500",
  FONT_BOLD: 'bold',
  FONT_NORMAL: 'normal'
};

export const FONT_STYLE = {
  FONT_NORMAL: "normal",
};

export const FONT_SPACING = {
  FONT_44: screenWidth * 0.0012,
  FONT_31: screenWidth * 0.0861,
};

export const FONT_FAMILY = {
  AirbnbCerealAppBold: 'AirbnbCerealApp-Bold',
  AirbnbCerealMedium: 'AirbnbCereal-Medium',
  AirbnbCerealLight: 'AirbnbCereal-Light',
  AirbnbCerealExtraBold: 'AirbnbCereal-ExtraBold',
  AirbnbCerealBook: 'AirbnbCereal-Book',
  AirbnbCerealBlack: 'AirbnbCereal-Black',
}

export const AirbnbCerealAppBold = 'AirbnbCerealApp-Bold';
export const AirbnbCerealMedium = 'AirbnbCereal-Medium';
export const AirbnbCerealLight = 'AirbnbCereal-Light';
export const AirbnbCerealExtraBold = 'AirbnbCereal-ExtraBold';
export const AirbnbCerealBook = 'AirbnbCereal-Book';
export const AirbnbCerealBlack = 'AirbnbCereal-Black';

//global variable

export const FCMTOKEN = null

//API End Points
export const MAINURL = 'http://apiptcfamilytrack.ptcschool.in/RestServiceImpl.svc/';
export const LOGIN = 'verifyuserloginmobilebased/';
export const SIGNUP = 'signupforadminoruser/';
export const JOINGROUP = 'joinnewtrackinggroup/'
export const ADDUSER = 'signupnewuserunderadminsgroup/';
export const USERDETAILS = 'getuserprofilewithaccountdetails/';
export const RECHARGEHISTORY = 'getrechargedonebyadmin/';
export const RECHARGELIST = 'getrechargeplansforchild/PTC/';
export const REFERALCODE = 'getreferralpointsbysharingapp/PTC/';
export const REFERALUSERS = 'getallreferreduser/PTC/';
export const REEDEMPOINTS = 'getpointspaidunpaidreport/PTC/';
export const SEARCHREDEEMPOINTS = 'getpointspaidunpaidreportbetweendates/';
export const SENDPAYMENTMETHOD = 'savepointsredeemrequestrechargebased/PTC/';
export const CHANGEPASSWORD = 'changeusrpassword';
export const PAYTMSAVEDETAILS = 'getsavedpaytmdetailsonly/';
export const PAYTMUPDATEDETAILS = 'savepaytmdetailsforadmin/';
export const BANKSAVEDETAILS = 'getsavedaccountorpaytmdetails/';
export const BANKUPDATEDETAILS = 'saveaccountdetailsforadmin/';
export const USERIMAGECHANGE = 'uploadphotoadminormember/';
export const GROUPLIST = 'getlistoftrackinggroupscreatedbyadmin/';
export const GROUPID = 'getlastlocationallusersofanygroup/';
export const GROUPNAME = 'savenewtrackinggroupdetails/'
export const CURRENTLOC = 'getlastlocationofuser/';
export const HISTORYUSER = 'getlocationhistoryofoneuser/'
export const SAVESAFEZONE = 'savesafezoneforchild/'
export const SAFEZONELIST = 'getlistofsafezonesforchild/';
export const DELETESAFEZONE = 'deletesafezoneforchild/'
export const DELETEGROUP = 'deactivatetrackinggroup/';
export const SAVECHILD = 'savechildinsafeorunsafezonealeart/';
export const SAVELOCATION = 'saveuserlocationhistory/';
export const SENDNOTIFICATION = 'sendnotificationtoringbellonusersmobile/';
export const USERFCMTOKEN = 'saveuserdevicefornotification/';
export const PAYTMCHECKSUM = 'generatefinalchecksumpaytm/XuCllw43y/'
export const PAYTMSAVEDATA = 'savepaytmresponseoftransection/';
export const SAVERECHARGEDETAILS = 'savechildrechargedetailsandactivateuser/';
export const OTPSENT = 'sentotptoresetpassword/';
export const FORGOTPASSWORD = 'changepasswordusingotp/'
// constants

export const USERPASSWORD = 'UserPassword='