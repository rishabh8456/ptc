import React from "react";
import { createSwitchNavigator, createStackNavigator, createAppContainer } from 'react-navigation';
import Slider from '../screens/Slider/slider';
import LogIn from '../screens/Login/login';
import Registration from '../screens/Registration/registration';
import HomeScreen from '../screens/HomeScreen/homeScreen';
import AuthLoadingScreen from '../screens/AuthLoading/authLoadingScreen';
import JoinGroup from '../screens/JoinGroup/joinGroup';
import AddUser from '../screens/AddUser/addUser';
import ProfileScreen from '../screens/Profile/profileScreen';
import RechargeHistory from '../screens/RechargeHistory/rechargeHistory';
import RechargePlans from '../screens/RechargePlans/rechargePlans';
import Pay from '../screens/Pay/pay';
import Invite from '../screens/Invite/invite';
import ShareNetwork from '../screens/ShareNetwork/shareNetwork';
import ReedemPoints from '../screens/ReedemPoints/reedemPoints';
import ChangeProfile from '../screens/ChangeImage/changeImage';
import GroupMenu from '../screens/groupMenu/groupMenu';
import UserMaps from '../screens/UserMaps/userMap';
import NewGroup from '../screens/NewGroup/newGroup';
import UserHistory from '../screens/UserHistory/userHistory';
import SafeZone from '../screens/SafeZone/safeZone';
import SafeZoneList from '../screens/SafeZoneList/safeZoneList';
import SuccessRecharge from '../screens/SuccessRecharge/successRecharge';
import OtpScreen from '../screens/OTP/otpScreen';
import ForgotPassword from '../screens/ForgotPassword/forgotPassword';
import TermsCondition from '../screens/TermsCondition/termsCondition';
import Policy from '../screens/Policy/policy';

const AuthStack = createStackNavigator({
  Slider: {
    screen: Slider,
    navigationOptions: {
      header: null
    }
  },
  LogIn: {
    screen: LogIn,
    navigationOptions: {
      header: null
    }
  },
  Registration: {
    screen: Registration,
    navigationOptions: {
      header: null
    }
  },
  OtpScreen: {
    screen: OtpScreen
  },
  ForgotPassword: {
    screen: ForgotPassword
  },
  TermsCondition: {
    screen: TermsCondition
  },
  Policy: {
    screen: Policy
  },

});

const AppStack = createStackNavigator({
  HomeScreen: {
    screen: HomeScreen,
    navigationOptions: {
      header: null
    }
  },
  RechargePlans: {
    screen: RechargePlans,
  },
  ShareNetwork: {
    screen: ShareNetwork,
  },
  RechargeHistory: {
    screen: RechargeHistory,
  },
  ProfileScreen: {
    screen: ProfileScreen,
  },
  JoinGroup: {
    screen: JoinGroup,
  },
  AddUser: {
    screen: AddUser,
  },
  Invite: {
    screen: Invite,
  },

  Pay: {
    screen: Pay,
  },
  ReedemPoints: {
    screen: ReedemPoints,
    navigationOptions: {
      header: null
    }
  },
  ChangeProfile: {
    screen: ChangeProfile,
  },
  GroupMenu: {
    screen: GroupMenu,
  },
  UserMaps: {
    screen: UserMaps,
  },
  NewGroup: {
    screen: NewGroup,
  },
  UserHistory: {
    screen: UserHistory,
  },
  SafeZone: {
    screen: SafeZone,
  },
  SafeZoneList: {
    screen: SafeZoneList,
  },
  SuccessRecharge: {
    screen: SuccessRecharge
  },

});

export default createAppContainer(createSwitchNavigator(
  {
    AuthLoadingScreen: AuthLoadingScreen,
    App: AppStack,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoadingScreen',
  }
));