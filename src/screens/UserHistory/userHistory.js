import React, { Component, Fragment } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  Image,
  Platform,
  PermissionsAndroid,
  ActivityIndicator,
  SafeAreaView,
  TextInput
} from "react-native";
import MapView, {
  Marker,
  AnimatedRegion,
  Polyline,
  PROVIDER_GOOGLE
} from "react-native-maps";
import * as constants from '../../common/constant';
const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width
const ASPECT_RATIO = screenWidth / screenHeight;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
import DatePicker from "react-native-datepicker";

let index = 0;
class UserHistory extends React.Component {

  static navigationOptions = ({ navigation }) => {

    const { userData } = navigation.state.params

    return {
      title: userData.username,
      headerTitleStyle: {
        color: 'white',
      },
      headerRight: (
        <View style={{ flex: 1 }} />
      ),
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.goBack()} style={{ marginLeft: 10, marginBottom: 5 }}>
          <Image style={{ height: screenHeight * 0.03, width: screenWidth * 0.06 }} source={require('../../assets/images/reply.png')} />
        </TouchableOpacity>
      ),
      headerStyle: {
        backgroundColor: 'rgb(0,100,89)',
      },
    };
  }

  constructor(props) {
    super(props);

    this.state = {
      latitude: 0,
      longitude: 0,
      startTimeEnable: false,
      startTime: '',
      endTime: '',
      startDate: '',
      userData: this.props.navigation.state.params.userData,
      coordinated: [],
      lastCoordinate: {
        latitude: 0,
        longitude: 0,
        locationArea: ''
      },
      isLoading: false,
      initialCoordinate: new AnimatedRegion({
        latitude: 0,
        longitude: 0,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      }),
      initialRegion: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      },
      lineCoordinate: [],
      linelat: 0,
      linelong: 0
    };
  }

  componentDidMount() {
    console.log(this.state.userData);

    this.setState({
      initialRegion: {
        latitude: Number(this.state.userData.Latitude),
        longitude: Number(this.state.userData.Longitude),
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      },
    })

    this.mapView.animateCamera({
      center: {
        latitude: Number(this.state.userData.Latitude),
        longitude: Number(this.state.userData.Longitude),
      },
      pitch: 45,
      heading: 90,
      altitude: 1000,
      zoom: 15
    })

  }

  sendDate() {
    const { userData } = this.state
    lineArray = []
    this.setState({
      coordinated: [],
      lineCoordinate: [],
      linelat: 0,
      linelong: 0,
      isLoading: true,
      lastCoordinate: {
        latitude: 0,
        longitude: 0,
        locationArea: ''
      },
      initialCoordinate: new AnimatedRegion({
        latitude: 0,
        longitude: 0,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      })
    }, () => {
      if (this.state.startTime !== '' && this.state.endTime !== '' && this.state.startDate !== '') {
        fetch(constants.MAINURL + constants.HISTORYUSER + userData.CompanyId + "/" + userData.UserId + '/' + this.state.startDate + '/' + this.state.startDate + '/' + this.state.startTime + '/' + this.state.endTime)
          .then(response => response.json())
          .then((responseJson) => {
            if (responseJson.GetLocationHistoryOfOneUserResult === undefined) {
              alert('No Record Found')
            } else {
              console.log('GetLocationHistoryOfOneUserResult1 -->', responseJson.GetLocationHistoryOfOneUserResult);
              let lastData = this.state.coordinated
              responseJson.GetLocationHistoryOfOneUserResult.map((item) => {
                lastData.push({ latitude: Number(item.Latitude), longitude: Number(item.Longitude), locationArea: item.LocationArea })
              })
              this.setState({
                coordinated: lastData
              }, () => {
                this.mapView.animateCamera({
                  center: {
                    latitude: Number(this.state.coordinated[0].latitude),
                    longitude: Number(this.state.coordinated[0].longitude),
                  },
                  pitch: 45,
                  heading: 90,
                  altitude: 1000,
                  // zoom: 15
                })
                var val = this.state.coordinated[this.state.coordinated.length - 1];
                this.setState({
                  lastCoordinate: {
                    latitude: val.latitude,
                    longitude: val.longitude,
                    locationArea: val.locationArea
                  },
                  initialCoordinate: new AnimatedRegion({
                    latitude: Number(this.state.coordinated[0].latitude),
                    longitude: Number(this.state.coordinated[0].longitude),
                    latitudeDelta: LATITUDE_DELTA,
                    longitudeDelta: LONGITUDE_DELTA
                  }),
                  isLoading: false
                }, () => {
                  index = 0
                  this.countDownTimer()
                })
              })
            }
          })
          .catch(error => console.log('error', error))
      } else {
        alert('Please select Details')
      }
    })
  }

  countDownTimer() {
    let data = setInterval(() => {
      if (index < this.state.coordinated.length - 1) {
        this.setState({
          initialCoordinate: new AnimatedRegion({
            latitude: Number(this.state.coordinated[index].latitude),
            longitude: Number(this.state.coordinated[index].longitude),
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          }),
          linelat: Number(this.state.coordinated[index].latitude),
          linelong: Number(this.state.coordinated[index].longitude)
        }, () => {
          const { linelat, linelong, lineCoordinate } = this.state;

          let newCoordinate = {
            latitude: linelat, longitude: linelong
          }
          this.setState({
            lineCoordinate: lineCoordinate.concat([newCoordinate]),
          }, () => {
            this.mapView.animateCamera({
              center: {
                latitude: linelat,
                longitude: linelong,
              },
              pitch: 45,
              heading: 90,
              altitude: 1000,
              // zoom: 15
            })
          })
          index = index + 1
        })
      } else {
        clearInterval(data)
      }
    }, 1000);
  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: 'rgb(0,100,89)', }} ></SafeAreaView>
        <View style={styles.container}>
          {
            <MapView
              style={styles.map}
              loadingEnabled={this.state.isLoading}
              showsBuildings={true}
              showsTraffic={true}
              ref={ref => (this.mapView = ref)}
              initialCamera={
                {
                  center: {
                    latitude: this.state.initialRegion.latitude,
                    longitude: this.state.initialRegion.longitude,
                  },
                  pitch: 90,
                  heading: 110,
                  zoom: 20,
                  altitude: 1000
                }
              }
            >
              {
                (this.state.lineCoordinate.length > 2) ?
                  <Polyline
                    coordinates={this.state.lineCoordinate}
                    strokeColor="red"
                    strokeWidth={4}
                  />
                  : null
              }
              {(this.state.coordinated.length > 0) ?
                <View>

                  <Marker
                    coordinate={{ latitude: this.state.coordinated[0].latitude, longitude: this.state.coordinated[0].longitude }}
                    title={this.state.initialCoordinate.locationArea}
                  >
                  </Marker>

                  <Marker.Animated
                    ref={marker => {
                      this.marker = marker;
                    }}
                    coordinate={this.state.initialCoordinate}
                  >
                    <Image style={styles.markerView} source={require('../../assets/images/iconMap.png')} />
                    <Image style={styles.markerImage} source={{ uri: this.state.userData.UserPhoto }} />
                  </Marker.Animated>
                </View>
                :
                <Marker
                  coordinate={{ latitude: Number(this.state.userData.Latitude), longitude: Number(this.state.userData.Longitude) }}
                  title={this.state.userData.LocationArea}
                >
                  <Image style={styles.markerView} source={require('../../assets/images/iconMap.png')} />
                  <Image style={styles.markerImage} source={{ uri: this.state.userData.UserPhoto }} />
                </Marker>
              }
            </MapView>

          }
        </View>
        <View style={styles.bottomView} >
          <View style={styles.view1} >
            <View style={styles.view2} >
              <DatePicker
                placeholder={'Start Time'}
                date={this.state.startTime}
                mode={'time'}
                format="hh.mmA"
                showIcon={false}
                onDateChange={(date) => this.setState({ startTime: date }, () => console.log(this.state.startTime))}
                confirmBtnText="Done"
                cancelBtnText="Cancel"
                customStyles={{
                  dateTouchBody: { alignItems: 'center', justifyContent: 'center', textAlign: 'center', height: screenHeight * 0.04, width: screenWidth * 0.48, borderWidth: 1, backgroundColor: 'white' },
                  dateInput: {
                    height: screenHeight * 0.04, width: screenWidth * 0.48, borderWidth: 0,
                  }
                }}
              />
            </View>
            <View style={styles.view3} >
              <DatePicker
                placeholder={'End Time'}
                date={(this.state.endTime !== null) ? this.state.endTime : null}
                mode={'time'}
                showIcon={false}
                format="hh.mmA"
                onDateChange={(date) => this.setState({ endTime: date }, () => console.log(this.state.endTime))}
                confirmBtnText="Done"
                cancelBtnText="Cancel"
                customStyles={{
                  dateTouchBody: { textAlign: 'center', height: screenHeight * 0.04, width: screenWidth * 0.48, borderWidth: 1, backgroundColor: 'white' },
                  dateInput: {
                    height: screenHeight * 0.04, width: screenWidth * 0.48, borderWidth: 0,
                  }
                }}
              />
            </View>
          </View>
          <View style={styles.view4} >
            <View style={styles.view2} >
              <DatePicker
                placeholder={'Date'}
                date={this.state.startDate}
                mode={'date'}
                format="YYYY-MM-DD"
                showIcon={false}
                onDateChange={(date) => this.setState({ startDate: date }, () => console.log(this.state.startDate))}
                confirmBtnText="Done"
                cancelBtnText="Cancel"
                customStyles={{
                  dateTouchBody: { alignItems: 'center', justifyContent: 'center', textAlign: 'center', height: screenHeight * 0.04, width: screenWidth * 0.48, borderWidth: 1, backgroundColor: 'white' },
                  dateInput: {
                    height: screenHeight * 0.04, width: screenWidth * 0.48, borderWidth: 0,
                  }
                }}
              />
            </View>
            <TouchableOpacity onPress={() => this.sendDate()} style={styles.bottomView1} >
              <Text style={{ color: 'white' }} >Search</Text>
            </TouchableOpacity>
          </View>
        </View>

      </Fragment >
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: "flex-end",
    alignItems: "center",
    flex: 1
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    flex: 1
  },
  bubble: {
    flex: 1,
    backgroundColor: "rgba(255,255,255,0.7)",
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20
  },
  latlng: {
    width: 200,
    alignItems: "stretch"
  },
  button: {
    width: 80,
    paddingHorizontal: 12,
    alignItems: "center",
    marginHorizontal: 10
  },
  buttonContainer: {
    flexDirection: "row",
    marginVertical: 20,
    backgroundColor: "transparent"
  },
  markerView: {
    position: 'absolute', resizeMode: "contain", height: 50, width: 50
  },
  markerImage: {
    marginTop: 3, marginLeft: 10, borderRadius: 20, height: 30, width: 30, resizeMode: "contain"
  },
  bottomView: {
    position: 'absolute', bottom: 0, alignSelf: 'flex-end', height: screenHeight * 0.15, width: '100%', backgroundColor: 'lightgray', paddingTop: screenHeight * 0.01
  },
  view1: {
    flexDirection: 'row', marginHorizontal: 5, justifyContent: 'space-between'
  },
  view2: {
    height: screenHeight * 0.04, width: screenWidth * 0.48,
  },
  view3: {
    height: screenHeight * 0.04, width: screenWidth * 0.48
  },
  view4: {
    textAlign: 'center', marginTop: 7, flexDirection: 'row', marginHorizontal: 5, justifyContent: 'space-between'
  },
  bottomView1: {
    borderRadius: 2, alignItems: 'center', justifyContent: 'center', height: screenHeight * 0.04, width: screenWidth * 0.48, borderWidth: 2, backgroundColor: 'rgb(44,198,45)', borderColor: 'white'
  }
});

export default UserHistory;