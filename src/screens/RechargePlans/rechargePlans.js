import React, { Component } from 'react';
import {
  View,
  Text,
  AsyncStorage,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
  StyleSheet,
  FlatList,
  ActivityIndicator
} from 'react-native'
import * as constants from '../../common/constant';
const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width
import styles from './style';

class RechargeHistory extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Recharge Plans',
      headerTitleStyle: {
        color: 'white',
      },
      headerRight: (
        <View style={{ flex: 1 }} />
      ),
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.goBack()} style={{ marginLeft: 10, marginBottom: 5 }}>
          <Image style={{ height: screenHeight * 0.03, width: screenWidth * 0.06 }} source={require('../../assets/images/reply.png')} />
        </TouchableOpacity>
      ),
      headerStyle: {
        backgroundColor: 'rgb(0,100,89)',
      },
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      isLoading: true
    }
  }

  componentDidMount() {
    fetch(constants.MAINURL + constants.RECHARGELIST + constants.LOCALUSER.UserId)
      .then(response => response.json())
      .then((responseJson) => {
        console.log('Login Response--->', responseJson);
        if (responseJson.Status !== 'Unsuccessful') {
          this.setState({
            dataSource: responseJson.GetRechargePlansForChildResult
          }, () => this.setState({ isLoading: false }))
        } else {
          this.setState({
            dataSource: []
          }, () => this.setState({ isLoading: false }))
        }
      })
      .catch(error => console.log(error))
  }

  renderItem(item, index) {
    return (
      <TouchableOpacity onPress={() => this.props.navigation.navigate('Pay', { rechargePlan: item })} style={styles.touchView} >
        <View style={styles.view1} >
          <View style={{ marginLeft: 10 }} >
            <View style={styles.view3} >
              <Text style={{ fontSize: constants.FONT_12, fontWeight: '500' }} >Plan for : </Text>
              <Text style={{ fontSize: constants.FONT_12, fontWeight: '500' }}>{item.RechargeTitle}</Text>
            </View>
            <View style={{ flexDirection: 'row', marginVertical: 5 }} >
              <Text style={{ fontSize: constants.FONT_12, }} >Amount : </Text>
              <Text style={{ fontSize: constants.FONT_12, }}>{item.RechargeAmount}</Text>
            </View>
          </View>

          <View style={styles.view2} >
            <Image style={{ height: 30, width: 30 }} source={require('../../assets/images/cart.png')} />
          </View>
        </View>
      </TouchableOpacity>
    )


  }

  render() {
    return (
      <View style={styles.MainContainer}>
        {
          (this.state.isLoading) ? <ActivityIndicator size="large" style={{ alignSelf: 'center' }} /> : (this.state.dataSource.length > 0) ?
            <FlatList
              style={{ marginTop: 10 }}
              data={this.state.dataSource}
              renderItem={({ item, index }) => this.renderItem(item, index)}
              extraData={this.state}
            /> : <Image style={{ resizeMode: 'contain' }} source={require('../../assets/images/result_no.png')} />
        }

      </View>
    );
  }
}

export default RechargeHistory;