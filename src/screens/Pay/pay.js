import React, { Component, Fragment } from 'react';
import {
  View,
  Text,
  AsyncStorage,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
  StyleSheet,
  FlatList,
  Alert,
  Platform,
  SafeAreaView,
} from 'react-native'
import * as constants from '../../common/constant';
import Modal from "react-native-modal";
import Paytm from '@philly25/react-native-paytm';
const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width

class Pay extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
      headerRight: (
        <View style={{ flex: 1 }} />
      ),
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.goBack()} style={{ marginLeft: 10, marginBottom: 5 }}>
          <Image style={{ height: screenHeight * 0.03, width: screenWidth * 0.06 }} source={require('../../assets/images/reply.png')} />
        </TouchableOpacity>
      ),
      headerStyle: {
        backgroundColor: 'rgb(0,96,85)',
        borderBottomWidth: 0,

      },
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      rechargePlan: this.props.navigation.state.params.rechargePlan,
      selfUser: false,
      otherUser: false,
      isChoosegroupName: 'Select Group',
      isGroupVisibile: false,
      groupDetails: [],
      chooseGroup: [],
      selectedGroup: null,
      userDetails: [],
      isUserVisibile: false,
      isUserName: 'Select User',
      selectedUser: null
    }
  }

  groupModal = () => {
    this.setState({ isGroupVisibile: !this.state.isGroupVisibile, selectedUser: null, selectedGroup: null, isUserName: 'Select User', isUserVisibile: false });
  };

  userModal = () => {
    this.setState({ isUserVisibile: !this.state.isUserVisibile, selectedUser: null, selectedGroup: null, isUserName: 'Select User', isUserVisibile: false });
  };

  componentWillMount() {
    Paytm.addListener(Paytm.Events.PAYTM_RESPONSE, this.onPayTmResponse);
  }

  componentWillUnmount() {
    Paytm.removeListener(Paytm.Events.PAYTM_RESPONSE, this.onPayTmResponse);
  }

  onPayTmResponse = (resp) => {
    const { STATUS, status, response } = resp;
    console.log('STATUS-->', STATUS);
    console.log('status-->', status);
    console.log('response-->', response);

    if (Platform.OS === 'ios') {
      if (status === 'Success') {
        const jsonResponse = JSON.parse(response);
        const { STATUS } = jsonResponse;
        if (STATUS && STATUS === 'TXN_SUCCESS') {
          this.savePayTmDetails(JSON.parse(response))
          this.saveRechargeDetails(JSON.parse(response))
          this.props.navigation.navigate('SuccessRecharge', { rechargeData: JSON.parse(response) })
          Alert.alert('PTC', 'Transaction Successfull')
        } else {
          Alert.alert('PTC', 'Transaction cancelled')
        }
      }
    } else {
      if (STATUS && STATUS === 'TXN_SUCCESS') {
      }
    }
  };

  savePayTmDetails(response) {
    fetch(constants.MAINURL + constants.PAYTMSAVEDATA + response.MID + '/' + response.ORDERID + '/' + response.TXNAMOUNT + '/' + response.CURRENCY + '/' + response.TXNID + '/' + response.BANKTXNID + '/' + response.STATUS + '/' + '/' + response.RESPCODE + '/' + response.RESPMSG + '/' + response.GATEWAYNAME + '/' + 'Axis' + '/' + response.PAYMENTMODE + '/?TXNDATE=' + response.TXNDATE)
      .then(response => response.json())
      .then((responseJson) => {
        console.log('savePayTmDetails--->', responseJson);
      })
      .catch(error => console.log(error))
  }

  saveRechargeDetails(response) {
    const { selectedUser, rechargePlan } = this.state;
    fetch(constants.MAINURL + constants.SAVERECHARGEDETAILS + response.TXNID + '/' + selectedUser.CompanyId + '/' + selectedUser.UserId + '/' + selectedUser.UserId + '/' + rechargePlan.RechargeAmount + '/' + rechargePlan.RechargeDays + '/?RechargeTitle=' + rechargePlan.RechargeTitle)
      .then(response => response.json())
      .then((responseJson) => {
        console.log('saveRechargeDetails--->', responseJson);

      })
      .catch(error => console.log(error))
  }

  isSelfUser() {
    this.setState({
      selfUser: true,
      otherUser: false,
    }, () => {
      this.setState({
        isUserVisibile: false,
        isGroupVisibile: false,
        selectedGroup: null,
        isChoosegroupName: 'Select Group',
        selectedUser: null,
        isUserName: 'Select User',
      })
    })
  }

  isOtherUser() {
    this.setState({
      otherUser: true,
      selfUser: false
    }, () => {
      this.setState({
        isUserVisibile: false,
        isGroupVisibile: false,
        selectedGroup: null,
        isChoosegroupName: 'Select Group',
        selectedUser: null,
        isUserName: 'Select User',
      })
    })
  }

  componentDidMount() {
    this.groupList()
  }

  groupList() {
    fetch(constants.MAINURL + constants.GROUPLIST + constants.LOCALUSER.CompanyId + "/" + constants.LOCALUSER.UserId)
      .then(response => response.json())
      .then((responseJson) => {
        if (responseJson.Status !== 'Unsuccessful') {
          this.setState({
            groupDetails: responseJson.GetListOfTrackingGroupsCreatedByAdminResult
          })
        } else {
          this.setState({
            groupDetails: []
          })
        }
      })
      .catch(error => console.log('error', error))
  }

  selectGroup(item) {
    this.setState({
      isGroupVisibile: false,
      selectedGroup: item
    }, () => {
      this.setState({ isChoosegroupName: item.TrackingGroupName })
      this.fetchUsers()
    })
  }

  fetchUsers() {
    fetch(constants.MAINURL + constants.GROUPID + constants.LOCALUSER.CompanyId + '/' + this.state.selectedGroup.TrackingGroupId)
      .then(response => response.json())
      .then((responseJson) => {
        console.log('groupData Response--->', responseJson);
        if (responseJson.Status === 'Unsuccessful') {
          this.setState({
            userDetails: [],
          })
        } else {
          this.setState({
            userDetails: responseJson.GetLastLocationOfAllUsersOfAnyGroupResult
          })
        }
      })
      .catch(error => console.log(error))
  }

  selectUser(item) {
    this.setState({
      isUserVisibile: false,
      selectedUser: item
    }, () => {
      this.setState({ isUserName: item.username })
    })
  }

  renderGroup(item, index) {
    return (
      <TouchableOpacity onPress={() => this.selectGroup(item)} style={{ paddingVertical: 10, alignItems: 'center', justifyContent: 'center', borderBottomWidth: 1 }} >
        <Text>{item.TrackingGroupName}</Text>
      </TouchableOpacity>
    )
  }

  renderUser(item, index) {
    return (
      <TouchableOpacity onPress={() => this.selectUser(item)} style={{ paddingVertical: 10, alignItems: 'center', justifyContent: 'center', borderBottomWidth: 1 }} >
        <Text>{item.username}</Text>
      </TouchableOpacity>
    )
  }

  sendRecharge() {
    if (this.state.selfUser || this.state.otherUser) {
      if (this.state.selfUser) {
        this.setState({
          selectedUser: constants.LOCALUSER
        })
        fetch(constants.MAINURL + constants.PAYTMCHECKSUM + constants.LOCALUSER.UserId + '/' + constants.LOCALUSER.UserMobile + '/' + this.state.rechargePlan.RechargeAmount + '/' + 'ptc.sachin@gmail.com')
          .then(response => response.json())
          .then((responseJson) => {
            if (responseJson.Status !== 'Unsuccessful') {
              console.log("Rechargeeeee--->", this.state.rechargePlan);
              console.log("Selected--->", this.state.selectedUser);

              this.callBackPaytm(responseJson)
            } else {
              Alert.alert('Unsuccessful')
            }
          })
          .catch(error => console.log('error', error))
      } else {
        console.log(this.state.selectedGroup);
        console.log(this.state.selectedUser);
        if (this.state.selectedGroup !== null) {
          if (this.state.selectedUser !== null) {
            fetch(constants.MAINURL + constants.PAYTMCHECKSUM + this.state.selectedUser.UserId + '/' + this.state.selectedUser.UserMobile + '/' + this.state.rechargePlan.RechargeAmount + '/' + 'ptc.sachin@gmail.com')
              .then(response => response.json())
              .then((responseJson) => {
                if (responseJson.Status !== 'Unsuccessful') {
                  this.callBackPaytm(responseJson)
                } else {
                  Alert.alert('Unsuccessful')
                }
              })
              .catch(error => console.log('error', error))
          } else {
            Alert.alert('PTC', 'Select Other Group User')
          }
        } else {
          Alert.alert('PTC', 'Select Other Group User')
        }
      }
    } else {
      Alert.alert('PTC', 'Please select users ')
    }
  }

  callBackPaytm(paytmConfig) {
    const details = {
      mode: 'Production',
      MID: paytmConfig.MID,
      INDUSTRY_TYPE_ID: paytmConfig.INDUSTRY_TYPE_ID,
      WEBSITE: paytmConfig.WEBSITE,
      CHANNEL_ID: paytmConfig.CHANNEL_ID,
      TXN_AMOUNT: paytmConfig.TXN_AMOUNT,
      ORDER_ID: paytmConfig.ORDER_ID,
      EMAIL: paytmConfig.EMAIL,
      MOBILE_NO: paytmConfig.MOBILE_NO,
      CUST_ID: paytmConfig.CUST_ID,
      CHECKSUMHASH: paytmConfig.checksumFinal,
      CALLBACK_URL: paytmConfig.CALLBACK_URL,
    };

    Paytm.startPayment(details);
  }

  render() {
    console.log("rechargePlan", this.state.rechargePlan);
    const { rechargePlan } = this.state;
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: "rgb(0,96,85)" }} ></SafeAreaView>
        <SafeAreaView style={{ flex: 1, backgroundColor: "orange" }}>
          <View style={{ flex: 1, backgroundColor: 'white' }} >
            <View style={{ flex: 1, backgroundColor: "rgb(0,96,85)", alignItems: 'center' }}>
              <Text style={{ fontSize: constants.FONT_20, color: "white", fontWeight: 'bold', textAlign: 'center', marginTop: screenHeight * 0.03 }} >Pay  !</Text>
              <View style={{ flexDirection: 'row', marginTop: screenHeight * 0.04, alignItems: 'center' }} >
                <Image style={{ resizeMode: 'contain', marginRight: 30 }} source={require('../../assets/images/rupeenew.png')} />
                <Text style={{ fontSize: constants.FONT_20, color: "white", fontWeight: 'bold', textAlign: 'center', }} >{rechargePlan.RechargeAmount}</Text>
              </View>
              <Text style={{ fontSize: constants.FONT_20, color: "white", fontWeight: 'bold', textAlign: 'center', marginTop: screenHeight * 0.05 }} >{'For ' + rechargePlan.RechargeTitle + ' Plan'}</Text>
            </View>
            <View style={{ flex: 2, backgroundColor: "rgb(239,239,238)", }}>
              <View style={{ marginTop: 10, alignSelf: 'center' }} >
                <Text style={{ fontSize: constants.FONT_12 }} >First select user then proceed</Text>
              </View>
              <View style={{ marginTop: screenHeight * 0.04, marginLeft: screenWidth * 0.1 }} >
                <TouchableOpacity onPress={() => {
                  this.isSelfUser()
                }} style={{ alignItems: 'center', flexDirection: 'row', marginBottom: 20, }} >
                  <Image style={{ height: 20, width: 20, marginRight: 20 }} source={(this.state.selfUser) ? require('../../assets/images/button_round_pink_T.png') : require('../../assets/images/white-round-md.png')} />
                  <Text style={{ fontSize: constants.FONT_14 }} >Self</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {
                  this.isOtherUser()
                }}
                  style={{ alignItems: 'center', flexDirection: 'row' }} >
                  <Image style={{ height: 20, width: 20, marginRight: 20 }} source={(this.state.otherUser) ? require('../../assets/images/button_round_pink_T.png') : require('../../assets/images/white-round-md.png')} />
                  <Text style={{ fontSize: constants.FONT_14 }}>Other group user</Text>
                </TouchableOpacity>
              </View>
              {
                (this.state.otherUser) ?
                  <View style={{ marginTop: screenHeight * 0.05, alignSelf: 'center' }} >
                    <TouchableOpacity onPress={() => { this.groupModal() }} style={{ alignItems: "center", justifyContent: 'center', backgroundColor: 'white', borderRadius: 10, borderWidth: 1, height: screenHeight * 0.059, width: screenWidth * 0.53 }} >
                      <Text>{this.state.isChoosegroupName}</Text>
                      <Modal onBackdropPress={() => this.setState({ isGroupVisibile: false })} isVisible={this.state.isGroupVisibile}>
                        {
                          (this.state.groupDetails.length) ?
                            <View style={{ borderRadius: 10, borderWidth: 1, backgroundColor: 'white', width: screenWidth * 0.8, alignSelf: 'center' }}>
                              <FlatList
                                data={this.state.groupDetails}
                                renderItem={({ item, index }) => this.renderGroup(item, index)}
                                extraData={this.state}
                              />
                            </View>
                            :
                            <View style={{ borderRadius: 10, borderWidth: 1, backgroundColor: 'white', width: screenWidth * 0.8, alignSelf: 'center', height: 40, alignItems: 'center', justifyContent: 'center' }}>
                              <Text>No groups for this user</Text>
                            </View>
                        }

                      </Modal>
                    </TouchableOpacity>
                  </View>
                  :
                  null
              }
              {
                (this.state.selectedGroup !== null) ?
                  <View style={{ marginTop: screenHeight * 0.05, alignSelf: 'center' }} >
                    <TouchableOpacity onPress={() => { this.userModal() }} style={{ alignItems: "center", justifyContent: 'center', backgroundColor: 'white', borderRadius: 10, borderWidth: 1, height: screenHeight * 0.059, width: screenWidth * 0.53 }} >
                      <Text>{this.state.isUserName}</Text>
                      <Modal onBackdropPress={() => this.setState({ isUserVisibile: false })} isVisible={this.state.isUserVisibile}>
                        {
                          (this.state.userDetails.length > 0) ?
                            <View style={{ borderRadius: 10, borderWidth: 1, backgroundColor: 'white', width: screenWidth * 0.8, alignSelf: 'center' }}>
                              <FlatList
                                data={this.state.userDetails}
                                renderItem={({ item, index }) => this.renderUser(item, index)}
                                extraData={this.state}
                              />
                            </View>
                            :
                            <View style={{ borderRadius: 10, borderWidth: 1, backgroundColor: 'white', width: screenWidth * 0.8, alignSelf: 'center', height: 40, alignItems: 'center', justifyContent: 'center' }}>
                              <Text>No users in this group</Text>
                            </View>
                        }

                      </Modal>
                    </TouchableOpacity>
                  </View>
                  :
                  null
              }
            </View>
            <TouchableOpacity onPress={() => this.sendRecharge()} style={{ justifyContent: 'center', alignItems: 'center', height: 30, width: screenWidth, backgroundColor: 'orange' }} >
              <Text style={{ fontSize: constants.FONT_16, color: "white", fontWeight: 'bold', textAlign: 'center', }} >Proceed to checkout</Text>
            </TouchableOpacity>
          </View >
        </SafeAreaView>
      </Fragment>
    );
  }
}

export default Pay;