import {
  Platform,
  StyleSheet,
  Dimensions
} from "react-native";
import * as constants from '../../common/constant';

const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width

module.exports = StyleSheet.create({
  MainView: {
    flex: 1, backgroundColor: 'rgb(218,219,202)', alignItems: 'center'
  },
  TextView: {
    alignItems: 'center', marginTop: screenHeight * 0.25
  },
  MobileText: {
    fontSize: constants.FONT_20, color: 'rgb(160,40,70)'
  },
  OTPText: {
    marginTop: 10, fontSize: constants.FONT_12,
  },
  TouchView: {
    marginVertical: screenHeight * 0.05,
    borderWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 20,
    borderColor: 'black',
    height: screenHeight * 0.06,
    width: screenWidth - 50,
    backgroundColor: 'white',
    marginHorizontal: 15
  },
  TextInput: {
    textAlign: 'center', height: screenHeight * 0.05, flex: 1,
  },
  OTPButton: {
    borderRadius: 30, height: screenHeight * 0.06, width: screenWidth * 0.55, backgroundColor: 'orange', alignItems: 'center', justifyContent: 'center'
  },
  OTPText1: {
    color: 'white', fontSize: screenHeight * 0.0259, fontWeight: 'bold'
  },
  navigateButton: {
    marginTop: screenHeight * 0.05
  }
});
