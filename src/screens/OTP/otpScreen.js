import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
  Alert
} from 'react-native'
import style from './style';
import * as constants from '../../common/constant';
const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width

class OtpScreen extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Forgot Password',
      headerTitleStyle: {
        color: 'white',
      },
      headerRight: (
        <View style={{ flex: 1 }} />
      ),
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.goBack()} style={{ marginLeft: 10, marginBottom: 5 }}>
          <Image style={{ height: screenHeight * 0.03, width: screenWidth * 0.06 }} source={require('../../assets/images/reply.png')} />
        </TouchableOpacity>
      ),
      headerStyle: {
        backgroundColor: 'rgb(0,100,89)',
      },
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      mobileNo: '',
      otpData: {},
      isNavigate: true
    }
  }

  sendOTP() {
    if (this.state.mobileNo !== null && this.state.mobileNo !== '') {
      console.log(constants.MAINURL + constants.OTPSENT + 'PTC/' + this.state.mobileNo);

      fetch(constants.MAINURL + constants.OTPSENT + 'PTC/' + this.state.mobileNo)
        .then(response => response.json())
        .then((responseJson) => {
          if (responseJson.Status === 'Success') {
            Alert.alert('OTP sent to your mobile number')
            this.setState({
              otpData: responseJson,
              isNavigate: false
            })
          } else {
            Alert.alert(responseJson.Status)
            this.setState({
              isNavigate: true
            })
          }
        })
        .catch(error => console.log(error))
    } else {
      Alert.alert('PTC', 'Please enter valid mobile number')
    }
  }

  render() {
    return (
      <View style={style.MainView}>
        <View style={style.TextView} >
          <Text style={style.MobileText}>Enter your mobile number</Text>
          <Text style={style.OTPText} >We will send you OTP message</Text>
        </View>
        <View style={style.TouchView} >
          <TextInput
            keyboardType={'number-pad'}
            returnKeyType={'done'}
            placeholder={'Mobile No.'}
            placeholderTextColor={'green'}
            value={this.state.mobileNo}
            maxLength={10}
            style={style.TextInput}
            onChangeText={(text) => this.setState({ mobileNo: text })}
          />
        </View>
        <TouchableOpacity onPress={() => this.sendOTP()} style={style.OTPButton} >
          <Text style={style.OTPText1} >Send OTP</Text>
        </TouchableOpacity>
        <TouchableOpacity disabled={this.state.isNavigate} onPress={() => this.props.navigation.navigate('ForgotPassword', { otpData: this.state.otpData })} style={style.navigateButton} >
          <Image source={require('../../assets/images/enter.png')} />
        </TouchableOpacity>
      </View>
    );
  }
}

export default OtpScreen;