import React, { Component } from 'react';
import {
  View,
  Text,
  AsyncStorage,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
  StyleSheet
} from 'react-native'
import * as constants from '../../common/constant';
const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width

class AddUser extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Add user',
      headerTitleStyle: {
        color: 'white',
      },
      headerRight: (
        <View style={{ flex: 1 }} />
      ),
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.goBack()} style={{ marginLeft: 10, marginBottom: 5 }}>
          <Image style={{ height: screenHeight * 0.03, width: screenWidth * 0.06 }} source={require('../../assets/images/reply.png')} />
        </TouchableOpacity>
      ),
      headerStyle: {
        backgroundColor: 'rgb(0,100,89)',
      },
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      userName: '',
      userPassword: '',
      userPhone: '',
    }
  }

  addUser() {
    console.log(constants.MAINURL + constants.ADDUSER + constants.LOCALUSER.CompanyId + '/' + this.props.navigation.state.params.data.TrackingGroupId + '/NA/' + this.state.userName + '/' + this.state.userPhone + '/?' + constants.USERPASSWORD + this.state.userPassword);

    fetch(constants.MAINURL + constants.ADDUSER + constants.LOCALUSER.CompanyId + '/' + this.props.navigation.state.params.data.TrackingGroupId + '/NA/' + this.state.userName + '/' + this.state.userPhone + '/?' + constants.USERPASSWORD + this.state.userPassword)
      .then(response => response.json())
      .then((responseJson) => {
        alert(responseJson.Status)
      })
      .catch(error => alert(error))
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "rgb(228,228,215)" }}>
        <View style={{ marginTop: screenHeight * 0.1, alignItems: 'center', alignSelf: 'center' }} >
          <Image style={{ resizeMode: 'contain' }} source={require('../../assets/images/userone.png')} />
          <Text style={{ fontWeight: 'bold', marginTop: screenHeight * 0.03, textAlign: 'center', color: 'white', fontSize: constants.FONT_12 }} >Family Group</Text>
        </View>
        <View style={{ marginTop: screenHeight * 0.05, alignSelf: 'center', alignItems: 'center' }} >
          <View style={styles.buttonView} >
            <Image style={styles.imageStyle} source={require('../../assets/images/manuser.png')} />
            <TextInput
              placeholder={'User Name'}
              placeholderTextColor={'darkgreen'}
              value={this.state.userName}
              style={{ textAlign: 'center', height: screenHeight * 0.05, flex: 1, marginRight: screenWidth * 0.1 }}
              onChangeText={(text) => this.setState({ userName: text })}
            />
          </View>
          <View style={[styles.buttonView, { marginTop: 10 }]} >
            <Image style={styles.imageStyle} source={require('../../assets/images/phonee.png')} />
            <TextInput
              placeholder={'Phone'}
              placeholderTextColor={'darkgreen'}
              style={{ textAlign: 'center', height: screenHeight * 0.05, flex: 1, marginRight: screenWidth * 0.1 }}
              keyboardType={'number-pad'}
              value={this.state.userPhone}
              maxLength={10}
              onChangeText={(text) => this.setState({ userPhone: text })}
            />
          </View>
          <View style={[styles.buttonView, { marginTop: 10 }]} >
            <Image style={styles.imageStyle} source={require('../../assets/images/key.png')} />
            <TextInput
              placeholder={'Password'}
              placeholderTextColor={'darkgreen'}
              style={{ textAlign: 'center', height: screenHeight * 0.05, flex: 1, marginRight: screenWidth * 0.1 }}
              value={this.state.userPassword}
              secureTextEntry={true}
              maxLength={10}
              onChangeText={(text) => this.setState({ userPassword: text })}
            />
          </View>
        </View>
        <View style={{ alignItems: 'center', marginTop: screenHeight * 0.05 }} >
          <TouchableOpacity onPress={() => this.addUser()} style={{ borderRadius: 30, height: screenHeight * 0.04, width: screenWidth * 0.55, backgroundColor: 'orange', alignItems: 'center', justifyContent: 'center' }} >
            <Text style={{ color: 'white', fontSize: screenHeight * 0.0259, fontWeight: 'bold' }} >Add User</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default AddUser;

const styles = StyleSheet.create({

  MainContainer: {
    flex: 1,
    backgroundColor: 'white'
  },
  buttonView: {
    borderWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 20,
    borderColor: 'black',
    height: screenHeight * 0.05,
    width: screenWidth - 50,
    backgroundColor: 'white',
    marginHorizontal: 15
  },
  imageStyle: {
    height: screenHeight * 0.05,
    width: screenWidth * 0.06,
    marginLeft: screenWidth * 0.06,
    marginRight: screenWidth * 0.1,
    resizeMode: 'contain'
  },
});