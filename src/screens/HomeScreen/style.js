import {
  Platform,
  StyleSheet,
  Dimensions
} from "react-native";
import * as constants from '../../common/constant';

const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width

module.exports = StyleSheet.create({
  safeArea1: {
    flex: 0, backgroundColor: 'rgb(0,100,89)',
  },
  view1: {
    flexDirection: 'row', height: screenHeight * 0.07, backgroundColor: 'rgb(0,100,89)', width: screenWidth, justifyContent: 'space-between', alignItems: 'center'
  },
  view2: {
    marginLeft: 10, marginBottom: 5
  },
  userImage: {
    borderRadius: 20, height: 40, width: 40
  },
  profileView: {
    borderRadius: 5, backgroundColor: 'white', height: screenHeight * 0.19, width: screenWidth * 0.42, alignSelf: 'center', alignItems: 'center'
  },
  profileimage: {
    marginTop: 5, height: screenHeight * 0.15, width: screenWidth * 0.38
  },
  profileimage1: {
    marginTop: 5, resizeMode: 'contain'
  },

  profileName: {
    marginTop: 5, fontSize: constants.FONT_12
  },
  profileName1: {
    marginTop: 15, fontSize: constants.FONT_12
  },
  groupMenu: {
    justifyContent: 'center', alignItems: 'center', paddingVertical: screenHeight * 0.01, paddingHorizontal: screenWidth * 0.1, borderRadius: 20, backgroundColor: 'white'
  },
  groupView1: {
    borderRadius: 10, backgroundColor: 'white', width: screenWidth * 0.8, alignSelf: 'center'
  },
  groupFlatList: {
    paddingVertical: 10, alignItems: 'center', justifyContent: 'center', borderBottomWidth: 1
  },
  sideView: {
    marginRight: 10, marginBottom: 5
  },
  sideView1: {
    borderRadius: 10, backgroundColor: 'white', width: screenWidth * 0.8, alignSelf: 'center'
  },
  itemView: {
    paddingVertical: 10, alignItems: 'center', justifyContent: 'center', borderBottomWidth: 1
  },
  markerView: {
    position: 'absolute', resizeMode: "contain", height: 50, width: 50
  },
  markerImage: {
    alignSelf: 'center', marginTop: 3, justifyContent: 'center', borderRadius: 20, height: 30, width: 30, resizeMode: "contain"
  },
  bottomView: {
    justifyContent: 'space-around', flexDirection: 'row', height: screenHeight * 0.1, width: '100%', backgroundColor: 'white', paddingTop: screenHeight * 0.01
  },
  bottomTouch: {
    alignItems: 'center'
  },
  bottomImage: {
    height: screenHeight * 0.05, width: screenWidth * 0.09, resizeMode: 'contain'
  }


});
