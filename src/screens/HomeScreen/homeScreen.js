import React, { Component, Fragment } from 'react';
import {
  View,
  Text,
  AsyncStorage,
  Image,
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
  Platform,
  StyleSheet,
  FlatList,
  ImageBackground
} from 'react-native'
import * as constants from '../../common/constant';
const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width
import MapView from 'react-native-maps';
import { Marker } from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
import Modal from "react-native-modal";
const ASPECT_RATIO = screenWidth / screenHeight;
const LATITUDE = 0;
const LONGITUDE = 0;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
import { Notification } from 'react-native-firebase';
import firebase from 'react-native-firebase';
import style from './style';


export const iPhoneX = (Platform.OS === "ios" && Dimensions.get("window").height >= 812 && Dimensions.get("window").width >= 375);


let _this = null;

class HomeScreen extends Component {

  constructor(props) {
    super(props)
    _this = this;
    this.state = {
      latitude: 0,
      longitude: 0,
      latitudeDelta: 0,
      longitudeDelta: 0,
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      selectedTab: 'Home',
      isModalVisible: false,
      isGroupVisibile: false,
      isProfileVisible: false,
      groupDetails: []
    }

  }

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };

  groupModal = () => {
    this.setState({ isGroupVisibile: !this.state.isGroupVisibile });
  };

  profileModal = () => {
    this.setState({ isProfileVisible: !this.state.isProfileVisible });
  };

  async NotificationDataMethod() {

    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
    } else {
      firebase.messaging().requestPermission()
        .then(() => {
          // User has authorised  
        })
        .catch(error => {
          console.log("Error--->", error);

        });
    }

    this.removeNotificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification: Notification) => {
      console.log('removeNotificationDisplayedListener', notification);

      // Process your notification as required
      // ANDROID: Remote notifications do not contain the channel ID. You will have to specify this manually if you'd like to re-display the notification.
    });
    this.removeNotificationListener = firebase.notifications().onNotification((notification: Notification) => {
      // Process your notification as required
      console.log('removeNotificationListener-->', notification);
    });

    this.removeNotificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen: NotificationOpen) => {
      // Get the action triggered by the notification being opened
      const action = notificationOpen.action;
      // Get information about the notification that was opened
      const notification: Notification = notificationOpen.notification;
    });
  }

  componentDidMount() {
    console.log(constants.FCMTOKEN);
    this.NotificationDataMethod()
    Geolocation.getCurrentPosition((position) => {
      console.log(position);
      this.setState({
        latitude: position.coords.latitude,
        longitude: position.coords.longitude,
        region: {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA,
        }
      }, () => this.callAPIforClubInfo());
    }, error => console.log(error),
      { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 }
    );

    this.TrackUser()

    AsyncStorage.getItem(constants.USERDATA).then((value) => {
      if (value) {
        constants.LOCALUSER = JSON.parse(value)
      }
    })
      .then(res => {
        console.log("Values-->", constants.LOCALUSER);
        if (constants.LOCALUSER.UserPhoto) {
          this.props.navigation.setParams({
            userImage: constants.LOCALUSER.UserPhoto
          })
        } else {
          this.props.navigation.setParams({
            userImage: 'https://img.icons8.com/ios/26/000000/user.png'
          })
        }
        this.setState({
          userData: constants.LOCALUSER
        }, () => {
          this.groupDetailsAPI()
        })
      });
  }

  TrackUser() {
    setInterval(() => {
      Geolocation.getCurrentPosition((position) => {
        console.log(position);
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          }
        }, () => this.callAPIforClubInfo());
      }, error => console.log(error),
        { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 }
      );
    }, 4000);

  }

  static refreshGroups() {
    if (_this !== null) {
      _this.groupDetailsAPI()
    }
  }

  groupDetailsAPI() {
    fetch(constants.MAINURL + constants.USERFCMTOKEN + constants.LOCALUSER.CompanyId + "/" + constants.LOCALUSER.UserId + '/' + constants.LOCALUSER.UserId + '/' + Platform.OS + '/?deviceregistrationid=' + constants.FCMTOKEN)
      .then(response => response.json())
      .then((responseJson) => {
        console.log('FCM-->', responseJson);
      })
      .catch(error => console.log('error', error))

    fetch(constants.MAINURL + constants.GROUPLIST + constants.LOCALUSER.CompanyId + "/" + constants.LOCALUSER.UserId)
      .then(response => response.json())
      .then((responseJson) => {
        console.log('groupData-->', responseJson.GetListOfTrackingGroupsCreatedByAdminResult);
        this.setState({
          groupDetails: responseJson.GetListOfTrackingGroupsCreatedByAdminResult
        })
      })
      .catch(error => console.log('error', error))
  }

  callAPIforClubInfo() {
    const { latitude, longitude } = this.state;
    fetch(constants.MAINURL + constants.SAVELOCATION + this.state.userData.CompanyId + "/" + this.state.userData.UserId + "/" + latitude + "/" + longitude + "/" + "?LocationArea=Delhi")
      .then(response => response.json())
      .then((responseJson) => {
        // console.log('Club Data-->', responseJson);
      })
      .catch(error => console.log('error', error))
  }

  OpenScreens(index) {
    if (index == 1) {
      this.props.navigation.navigate('ProfileScreen')
    } else if (index == 2) {
      this.props.navigation.navigate('RechargePlans')
    } else if (index == 3) {
      this.props.navigation.navigate('ShareNetwork')
    } else if (index == 4) {
      this.props.navigation.navigate('RechargeHistory')
    } else if (index == 5) {
      this.props.navigation.navigate('ReedemPoints')
    } else if (index == 6) {
      this.props.navigation.navigate('Invite')
    } else if (index == 7) {
      AsyncStorage.clear()
      this.props.navigation.navigate('Auth')
    }
    else {
      this.setState({ isModalVisible: false });
    }
    this.setState({ isModalVisible: false });
  }

  navigateToGroup(item) {
    this.setState({
      isGroupVisibile: false
    }, () => {
      this.props.navigation.navigate('GroupMenu', { data: item.TrackingGroupId, name: item.TrackingGroupName })
    })
  }

  renderItem(item, index) {
    return (
      <TouchableOpacity onPress={() => this.navigateToGroup(item)} style={style.groupFlatList} >
        <Text>{item.TrackingGroupName}</Text>
      </TouchableOpacity>
    )
  }

  render() {
    console.log("this.state.userData--->", this.state.userData);

    return (
      <Fragment>
        <SafeAreaView style={style.safeArea1} ></SafeAreaView>
        <View style={style.view1} >
          <View  >
            <View style={style.view2}>
              {(this.state.userData)
                ?
                <TouchableOpacity onPress={() => { this.profileModal() }}>
                  {
                    (this.state.userData['UserPhoto']) ?
                      <Image style={style.userImage} source={{ uri: this.state.userData.UserPhoto }} />
                      :
                      <Image style={style.userImage} source={require('../../assets/images/profileeeepic.png')} />
                  }

                  <Modal onBackdropPress={() => this.setState({ isProfileVisible: false })} isVisible={this.state.isProfileVisible}>
                    <View style={style.profileView} >
                      {
                        (this.state.userData['UserPhoto']) ?
                          <View style={{ alignItems: 'center', justifyContent: "center" }} >
                            <Image style={style.profileimage} source={{ uri: this.state.userData.UserPhoto }} />
                            <Text style={style.profileName} >{this.state.userData.UserName}</Text>
                          </View>
                          :
                          <View style={{ alignItems: 'center', justifyContent: "center" }} >
                            <Image style={style.profileimage1} source={require('../../assets/images/profileeeepic.png')} />
                            <Text style={style.profileName1} >{this.state.userData.username}</Text>
                          </View>
                      }

                    </View>
                  </Modal>
                </TouchableOpacity>
                :
                <Image style={style.userImage} source={{ uri: 'https://img.icons8.com/ios/26/000000/user.png' }} />
              }
            </View>
          </View>
          <View>
            {
              (this.state.groupDetails !== undefined && this.state.groupDetails.length > 0) ?
                <TouchableOpacity onPress={() => { this.groupModal() }} style={style.groupMenu} >
                  <Text>Select Group</Text>
                  <Modal onBackdropPress={() => this.setState({ isGroupVisibile: false })} isVisible={this.state.isGroupVisibile}>
                    <View style={style.groupView1}>
                      {
                        (this.state.groupDetails !== undefined) ?
                          <FlatList
                            data={this.state.groupDetails}
                            renderItem={({ item, index }) => this.renderItem(item, index)}
                            extraData={this.state}
                          />
                          :
                          <Image style={{ alignSelf: 'center', resizeMode: 'center' }} source={require('../../assets/images/result_no.png')} />
                      }

                    </View>
                  </Modal>
                </TouchableOpacity>
                : null
            }

          </View>
          <TouchableOpacity onPress={() => { this.toggleModal() }} >
            <View style={style.sideView}>
              <Image style={{ height: 20, width: 20 }} source={require('../../assets/images/dotssss.png')} />
              <Modal onBackdropPress={() => this.setState({ isModalVisible: false })} isVisible={this.state.isModalVisible}>
                <View style={style.sideView1}>
                  <TouchableOpacity onPress={() => this.OpenScreens(1)} style={style.itemView} >
                    <Text>Profile</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.OpenScreens(2)} style={style.itemView} >
                    <Text>Recharge Plans</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.OpenScreens(3)} style={style.itemView} >
                    <Text>Share Network</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.OpenScreens(4)} style={style.itemView}>
                    <Text>Recharge History</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.OpenScreens(5)} style={style.itemView}>
                    <Text>Reedem Points</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.OpenScreens(6)} style={style.itemView} >
                    <Text>Reffral code</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.OpenScreens(7)} style={style.itemView} >
                    <Text>Logout</Text>
                  </TouchableOpacity>
                </View>
              </Modal>
            </View>
          </TouchableOpacity>
        </View>
        <MapView
          style={{ flex: 1 }}
          region={this.state.region}
        >
          <Marker
            coordinate={this.state.region}
            title={'My Location'}
          >
            <View style={{ alignItems: 'center' }} >
              <Image style={style.markerView} source={require('../../assets/images/iconMap.png')} />
              <Image style={style.markerImage} source={{ uri: constants.LOCALUSER.UserPhoto }} />
            </View>

          </Marker>
        </MapView>
        <View style={style.bottomView} >
          <TouchableOpacity onPress={() => this.props.navigation.navigate('JoinGroup')} style={style.bottomTouch} >
            <Image style={style.bottomImage} source={require('../../assets/images/small_user.png')} />
            <Text style={{}} >Add</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('Invite')} style={style.bottomTouch} >
            <Image style={style.bottomImage} source={require('../../assets/images/reffral_icon.png')} />
            <Text style={{ marginLeft: 10 }}>Invite</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('ReedemPoints')} style={style.bottomTouch} >
            <Image style={style.bottomImage} source={require('../../assets/images/offer.png')} />
            <Text>Points</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('RechargePlans')} style={style.bottomTouch} >
            <Image style={style.bottomImage} source={require('../../assets/images/recharge_newww.png')} />
            <Text>Recharge</Text>
          </TouchableOpacity>
        </View>
      </Fragment >
    );
  }
}

export default HomeScreen;

