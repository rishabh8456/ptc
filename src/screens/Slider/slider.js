import React, { Component } from 'react';

import {
  View,
  Platform,
  StyleSheet,
  Dimensions,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
  SafeAreaView
} from 'react-native'
import Slideshow from 'react-native-slideshow';
import PropTypes from 'prop-types';
import * as Common from '../../common/constant';
import Swiper from 'react-native-swiper'

const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width

class Slider extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }
  render() {
    return (
      <SafeAreaView style={styles.MainContainer}>
        <Swiper activeDotColor={'white'} paginationStyle={{ backgroundColor: 'orange', bottom: 0, height: 30 }} style={styles.wrapper} showsPagination={true} showsButtons={false}>
          <Image style={{ height: '100%', width: '100%' }} source={require('../../assets/images/app_screensone.png')} />
          <Image style={{ height: '100%', width: '100%' }} source={require('../../assets/images/app_screenstwo.png')} />
          <Image style={{ height: '100%', width: '100%' }} source={require('../../assets/images/app_screensthree.png')} />
          <ImageBackground style={{ alignItems: "center", height: '100%', width: '100%' }} source={require('../../assets/images/app_screensfour.png')} >
            <TouchableOpacity onPress={() => this.props.navigation.navigate('LogIn')} style={{ height: screenHeight * 0.04, width: screenWidth * 0.25, position: 'absolute', borderRadius: 15, bottom: screenHeight * 0.1, alignItems: 'flex-end', backgroundColor: 'orange', alignItems: 'center', justifyContent: 'center' }} >
              <Text style={{ color: 'white' }} >Enter</Text>
            </TouchableOpacity>
          </ImageBackground>
        </Swiper>
      </SafeAreaView>
    );
  }
}

export default Slider;

const styles = StyleSheet.create({

  MainContainer: {
    flex: 1,
    backgroundColor: 'white'

  },
  wrapper: {},
  slide1: {
    flex: 1,
  },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#97CAE5'
  },
  slide3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#92BBD9'
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold'
  }

});