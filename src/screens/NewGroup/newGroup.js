import React, { Component } from 'react';

import {
  View,
  TouchableOpacity,
  Image,
  Dimensions,
  Text,
  TextInput,
  FlatList,
  Alert,
  ActivityIndicator

} from 'react-native';

import * as constants from '../../common/constant';
import Share from 'react-native-share';
const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width
import Modal from "react-native-modal";
import HomeScreen from '../HomeScreen/homeScreen';

class NewGroup extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Create New Group',
      headerTitleStyle: {
        color: 'white',
      },
      headerRight: (
        <TouchableOpacity onPress={() => navigation.goBack()} style={{ marginRight: 10, marginBottom: 5 }}>
          <Image style={{ height: screenHeight * 0.03, width: screenWidth * 0.06 }} source={require('../../assets/images/whithome.png')} />
        </TouchableOpacity>

      ),
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.goBack()} style={{ marginLeft: 10, marginBottom: 5 }}>
          <Image style={{ height: screenHeight * 0.03, width: screenWidth * 0.06 }} source={require('../../assets/images/reply.png')} />
        </TouchableOpacity>
      ),
      headerStyle: {
        backgroundColor: 'rgb(0,100,89)',
      },
    };
  }


  constructor(props) {
    super(props);
    this.state = {
      groupDetails: [],
      isChoose: false,
      isWrite: false,
      groupName: null,
      isGroupVisibile: false,
      chooseGroup: [
        { id: '1', name: 'Friends' },
        { id: '2', name: 'Family' },
        { id: '3', name: 'Office' },
        { id: '4', name: 'Staff' }
      ],
      isChoosegroupName: 'Choose Group Name',
      isLoading: true
    }
  }

  groupModal = () => {
    this.setState({ isGroupVisibile: !this.state.isGroupVisibile });
  };

  componentDidMount() {
    this.groupDetailsAPI()
  }

  groupDetailsAPI() {
    console.log(constants.MAINURL + constants.GROUPLIST + constants.LOCALUSER.CompanyId + "/" + constants.LOCALUSER.UserId)
    fetch(constants.MAINURL + constants.GROUPLIST + constants.LOCALUSER.CompanyId + "/" + constants.LOCALUSER.UserId)
      .then(response => response.json())
      .then((responseJson) => {
        console.log('groupData-->', responseJson.GetListOfTrackingGroupsCreatedByAdminResult);
        this.setState({
          groupDetails: responseJson.GetListOfTrackingGroupsCreatedByAdminResult,
          isLoading: false
        }, () => {
          HomeScreen.refreshGroups()
        })
      })
      .catch(error => console.log('error', error), this.setState({ isLoading: false }))
  }

  deleteGroup(item, index) {
    let currentData = this.state.groupDetails
    fetch(constants.MAINURL + constants.DELETEGROUP + constants.LOCALUSER.CompanyId + "/" + constants.LOCALUSER.UserId + '/' + item.TrackingGroupId)
      .then(response => response.json())
      .then((responseJson) => {
        console.log('Delete groupData-->', responseJson);
        currentData.splice(index, 1)
        this.setState({
          groupDetails: currentData
        }, () => {
          HomeScreen.refreshGroups()
        })
      })
      .catch(error => console.log('error', error))
  }

  renderItem(item, index) {
    const shareOptions = {
      title: 'Share via',
      message: 'Group Code : ' + item.TrackingGroupId,
    };
    return (
      <View style={{ paddingVertical: 5, marginBottom: 10, backgroundColor: 'white', width: screenWidth - 20, marginHorizontal: 10, borderRadius: 5, }} >
        <View style={{ flexDirection: 'row' }} >
          <Text style={{ marginLeft: 7, color: 'orange', flex: 1 }} >{item.TrackingGroupName}</Text>
          <TouchableOpacity onPress={() => this.deleteGroup(item, index)} >
            <Image style={{ marginRight: 10, height: 10, width: 10, alignSelf: 'flex-end' }} source={require('../../assets/images/delete.png')} />
          </TouchableOpacity>
        </View>

        <Text style={{ marginLeft: 7, marginTop: 5 }} >{'Code : ' + item.TrackingGroupId}</Text>
        <View style={{ marginHorizontal: 5, marginTop: 5, flexDirection: 'row', justifyContent: 'space-around', }} >
          <TouchableOpacity onPress={() => Share.open(shareOptions)} style={{ marginRight: 5, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-evenly', paddingVertical: 10, paddingHorizontal: 10, flex: 1, backgroundColor: "rgb(247,246,242)" }} >
            <Image style={{ height: 20, width: 20, }} source={require('../../assets/images/share_code.png')} />
            <Text>Share Code</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.props.navigation.navigate('AddUser', { data: item })} style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-evenly', paddingVertical: 10, paddingHorizontal: 10, flex: 1, backgroundColor: "rgb(247,246,242)" }} >
            <Image style={{ height: 20, width: 20, }} source={require('../../assets/images/small_user.png')} />
            <Text>Add user</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  isChoose() {
    this.setState({
      isChoose: true,
      isWrite: false
    })
  }

  isWrite() {
    this.setState({
      isChoose: false,
      isWrite: true
    })
  }

  createGroup() {
    if (this.state.isChoose || this.state.isWrite) {
      if (this.state.isChoose) {
        if (this.state.isChoosegroupName !== 'Choose Group Name') {
          console.log(constants.MAINURL + constants.GROUPNAME + constants.LOCALUSER.CompanyId + "/" + constants.LOCALUSER.UserId + '/' + this.state.isChoosegroupName)
          fetch(constants.MAINURL + constants.GROUPNAME + constants.LOCALUSER.CompanyId + "/" + constants.LOCALUSER.UserId + '/' + this.state.isChoosegroupName)
            .then(response => response.json())
            .then((responseJson) => {
              if (responseJson.Status !== 'Success') {
                Alert.alert('PTC', responseJson.Status)
              }
              console.log('new groupData-->', responseJson);
              this.groupDetailsAPI()
            })
            .catch(error => console.log('error', error))
        } else {
          Alert.alert('PTC', 'Please select Options')
        }
      } else {
        if (this.state.groupName !== '' && this.state.groupName !== null) {
          fetch(constants.MAINURL + constants.GROUPNAME + constants.LOCALUSER.CompanyId + "/" + constants.LOCALUSER.UserId + '/' + this.state.groupName)
            .then(response => response.json())
            .then((responseJson) => {
              if (responseJson.Status !== 'Success') {
                Alert.alert('PTC', responseJson.Status)
              }
              console.log('new groupData-->', responseJson);
              this.groupDetailsAPI()
            })
            .catch(error => console.log('error', error))
        } else {
          Alert.alert('PTC', 'Please write group name')
        }
      }
    } else {
      Alert.alert('PTC', 'Please select options ')
    }
  }

  navigateToGroup(item) {
    this.setState({
      isGroupVisibile: false
    }, () => {
      this.setState({ isChoosegroupName: item.name })
    })

  }

  renderGroup(item, index) {
    return (
      <TouchableOpacity onPress={() => this.navigateToGroup(item)} style={{ paddingVertical: 10, alignItems: 'center', justifyContent: 'center', borderBottomWidth: 1 }} >
        <Text>{item.name}</Text>
      </TouchableOpacity>
    )
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "rgb(247,246,242)", }} >
        <View style={{ backgroundColor: "rgb(247,246,242)", alignItems: 'center', justifyContent: 'center' }} >
          <TouchableOpacity onPress={() => this.createGroup()} style={{ right: 20, alignItems: 'center', position: 'absolute', alignSelf: 'flex-end', height: screenHeight * 0.07, width: screenWidth * 0.13, backgroundColor: 'green', justifyContent: 'center' }} >
            <Image style={{ height: screenHeight * 0.04, width: screenWidth * 0.08 }} source={require('../../assets/images/add_btnn.png')} />
          </TouchableOpacity>
          <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 15 }} >
            <TouchableOpacity onPress={() => this.isChoose()} >
              <Image style={{ height: 20, width: 20 }} source={(this.state.isChoose) ? require('../../assets/images/button_round_pink_T.png') : require('../../assets/images/white-round-md.png')} />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => { this.groupModal() }} style={{ alignItems: "center", justifyContent: 'center', backgroundColor: 'white', marginRight: screenWidth * 0.1, marginLeft: screenWidth * 0.05, borderWidth: 1, height: screenHeight * 0.059, width: screenWidth * 0.53 }} >
              <Text>{this.state.isChoosegroupName}</Text>
              <Modal onBackdropPress={() => this.setState({ isGroupVisibile: false })} isVisible={this.state.isGroupVisibile}>
                <View style={{ borderRadius: 10, borderWidth: 1, backgroundColor: 'white', width: screenWidth * 0.8, alignSelf: 'center' }}>
                  <FlatList
                    data={this.state.chooseGroup}
                    renderItem={({ item, index }) => this.renderGroup(item, index)}
                    extraData={this.state}
                  />
                </View>
              </Modal>
            </TouchableOpacity>
          </View>
          <Text style={{ marginVertical: 5 }} >or</Text>
          <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 20 }} >
            <TouchableOpacity onPress={() => this.isWrite()} >
              <Image style={{ height: 20, width: 20, }} source={(this.state.isWrite) ? require('../../assets/images/button_round_pink_T.png') : require('../../assets/images/white-round-md.png')} />
            </TouchableOpacity>
            <TextInput value={this.state.groupName} onChangeText={(text) => this.setState({
              groupName: text
            })} placeholder={'write Group Name'} style={{ backgroundColor: 'white', textAlign: 'center', marginRight: screenWidth * 0.1, marginLeft: screenWidth * 0.05, borderWidth: 1, height: screenHeight * 0.059, width: screenWidth * 0.53 }} >
            </TextInput>
          </View>


        </View>
        {
          (this.state.isLoading) ? <ActivityIndicator size="large" style={{ alignSelf: 'center' }} /> :
            (this.state.groupDetails !== null && this.state.groupDetails !== undefined && this.state.groupDetails.length > 0) ?
              <FlatList
                style={{ marginTop: 10 }}
                data={this.state.groupDetails}
                renderItem={({ item, index }) => this.renderItem(item, index)}
                extraData={this.state}
              />
              :
              <Image style={{ alignSelf: 'center', marginTop: screenHeight * 0.1, resizeMode: 'center' }} source={require('../../assets/images/result_no.png')} />
        }

      </View>
    );
  }
}

export default NewGroup;