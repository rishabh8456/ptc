import React, { Component } from 'react';
import {
  View,
  Text,
  AsyncStorage,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
  StyleSheet,
  FlatList,
  ActivityIndicator
} from 'react-native'
import * as constants from '../../common/constant';
const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width
import style from './style'

class ShareNetwork extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Share Network',
      headerTitleStyle: {
        color: 'white',
      },
      headerRight: (
        <View style={{ flex: 1 }} />
      ),
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.goBack()} style={{ marginLeft: 10, marginBottom: 5 }}>
          <Image style={{ height: screenHeight * 0.03, width: screenWidth * 0.06 }} source={require('../../assets/images/reply.png')} />
        </TouchableOpacity>
      ),
      headerStyle: {
        backgroundColor: 'rgb(0,100,89)',
      },
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      isLoading: true
    }
  }

  componentDidMount() {


    fetch(constants.MAINURL + constants.REFERALUSERS + constants.LOCALUSER.UserId)
      .then(response => response.json())
      .then((responseJson) => {
        console.log('Login Response--->', responseJson);
        if (responseJson.Status !== 'Unsuccessful') {
          this.setState({
            dataSource: responseJson.GetListOfAllReferredUsersResult
          }, () => this.setState({ isLoading: false }))
        } else {
          this.setState({
            dataSource: []
          }, () => this.setState({ isLoading: false }))
        }

      })
      .catch(error => this.setState({ isLoading: false }))
  }

  renderItem(item, index) {
    return (
      <View style={style.touchView} >
        <View style={style.view1} >
          <View style={style.view3} >
            <View style={{}} >
              <Text style={style.orangeText} >{item.username}</Text>
              <View style={style.view4} >
                <Text style={{ fontSize: constants.FONT_12, }}>{'Mobile: '}</Text>
                <Text style={{ fontSize: constants.FONT_12, }}>{item.UserMobile}</Text>
              </View>
              <Text style={{ fontSize: constants.FONT_12, fontWeight: 'bold' }}>{item.RecordInsertDate}</Text>
            </View>
          </View>
          <View style={style.view2} >
            {
              (item.ActiveStatus === 'Active') ?
                <Image style={{ height: screenHeight * 0.05, width: screenWidth * 0.1 }} source={require('../../assets/images/green_icon.png')} />
                :
                null
            }

          </View>
        </View>
      </View >
    )


  }

  render() {
    return (
      <View style={style.MainContainer}>
        {
          (this.state.isLoading) ? <ActivityIndicator size="large" style={{ alignSelf: 'center' }} /> :
            (this.state.dataSource.length > 0) ?
              <FlatList
                style={{ marginTop: 10 }}
                data={this.state.dataSource}
                renderItem={({ item, index }) => this.renderItem(item, index)}
                extraData={this.state}
              />
              :
              <Image style={{ resizeMode: 'contain' }} source={require('../../assets/images/result_no.png')} />
        }
      </View >
    );
  }
}

export default ShareNetwork;