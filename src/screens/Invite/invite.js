import React, { Component } from 'react';
import {
  View,
  Text,
  AsyncStorage,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
  StyleSheet,
  FlatList
} from 'react-native'
import * as constants from '../../common/constant';
const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width
import style from './style';

class Invite extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Invite code',
      headerTitleStyle: {
        color: 'white',
      },
      headerRight: (
        <View style={{ flex: 1 }} />
      ),
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.goBack()} style={{ marginLeft: 10, marginBottom: 5 }}>
          <Image style={{ height: screenHeight * 0.03, width: screenWidth * 0.06 }} source={require('../../assets/images/reply.png')} />
        </TouchableOpacity>
      ),
      headerStyle: {
        backgroundColor: 'rgb(0,100,89)',
      },
    };
  }

  componentDidMount() {
    fetch(constants.MAINURL + constants.REFERALCODE + constants.LOCALUSER.UserId)
      .then(response => response.json())
      .then((responseJson) => {
        console.log('GetReferralPointsEarnedByAdminResult Response--->', responseJson);
        this.setState({
          data: responseJson.GetReferralPointsEarnedByAdminResult
        })

      })
      .catch(error => console.log(error))
  }

  constructor(props) {
    super(props);
    this.state = {
      data: []
    }
  }
  render() {
    return (
      <View style={style.mainView} >
        <View style={style.view1} >
          <View style={style.view2} >
            <View style={style.view3} >
              <Image style={{ resizeMode: 'contain' }} source={require('../../assets/images/dollar.png')} />
              <Text style={style.pointText} >{this.state.data.TotalPointsEarned}</Text>
            </View>
            <Text>Total Points</Text>
          </View>
        </View>
        <View style={style.view4} >
          <View style={style.view5} >
            <Image style={style.giftImage} source={require('../../assets/images/giftt.png')} />
          </View>
          <View style={style.view6} >
            <Text style={style.referalText} >{constants.LOCALUSER.AdminReffralCode}</Text>
            <Text style={style.codetext} >Share Your Code With Your Friends and get refferal point.</Text>
          </View>
          <View style={style.view6} >
            <TouchableOpacity style={style.shareTouch} >
              <Text style={style.shareText} >Share Invite Code</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View >
    );
  }
}

export default Invite;