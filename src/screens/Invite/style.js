import {
  Platform,
  StyleSheet,
  Dimensions
} from "react-native";
import * as constants from '../../common/constant';

const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width

module.exports = StyleSheet.create({
  mainView: {
    flex: 1, backgroundColor: 'white'
  },
  view1: {
    flex: 1, backgroundColor: 'rgb(253,209,53)', justifyContent: 'center'
  },
  view2: {
    marginLeft: screenWidth * 0.08, justifyContent: 'center'
  },
  view3: {
    flexDirection: 'row', alignItems: 'center'
  },
  pointText: {
    color: 'black', fontSize: screenHeight * 0.07
  },
  view4: {
    flex: 2, backgroundColor: 'white'
  },
  view5: {
    marginTop: screenHeight * 0.1, alignSelf: 'center'
  },
  giftImage: {
    height: screenHeight * 0.15, width: screenWidth * 0.3
  },
  view5: {
    marginTop: screenHeight * 0.06, alignSelf: 'center'
  },
  referalText: {
    textAlign: 'center', fontSize: constants.FONT_20, fontWeight: 'bold'
  },
  codetext: {
    marginHorizontal: 20, marginTop: 10, textAlign: 'center', fontSize: constants.FONT_16,
  },
  view6: {
    marginTop: screenHeight * 0.06, alignSelf: 'center'
  },
  shareTouch: {
    paddingVertical: 8, paddingHorizontal: screenWidth * 0.1, backgroundColor: 'orange', borderRadius: 15
  },
  shareText: {
    color: 'white', fontSize: constants.FONT_16, fontWeight: 'bold'
  }

});
