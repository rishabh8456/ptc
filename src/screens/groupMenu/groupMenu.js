import React from "react";
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  Dimensions,
  ActivityIndicator,
  FlatList
} from "react-native";
import * as constants from '../../common/constant';
const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width
import MapView from 'react-native-maps';
import { Marker } from 'react-native-maps';
const ASPECT_RATIO = screenWidth / screenHeight;
const LATITUDE = 0;
const LONGITUDE = 0;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
import style from './style';

class GroupMenu extends React.Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.state.params.name,
      headerTitleStyle: {
        color: 'white',
      },
      headerRight: (
        <TouchableOpacity onPress={() => navigation.goBack()} style={{ marginRight: 10, marginBottom: 5 }}>
          <Image style={{ height: screenHeight * 0.03, width: screenWidth * 0.06 }} source={require('../../assets/images/whithome.png')} />
        </TouchableOpacity>

      ),
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.goBack()} style={{ marginLeft: 10, marginBottom: 5 }}>
          <Image style={{ height: screenHeight * 0.03, width: screenWidth * 0.06 }} source={require('../../assets/images/reply.png')} />
        </TouchableOpacity>
      ),
      headerStyle: {
        backgroundColor: 'rgb(0,100,89)',
      },
    };
  }

  constructor(props) {
    super(props);

    this.state = {
      groupData: [],
      isLoading: true,
      groupId: this.props.navigation.state.params.data,
      latitude: 0,
      longitude: 0,
      latitudeDelta: 0,
      longitudeDelta: 0,
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
    };
  }

  componentDidMount() {
    console.log(constants.MAINURL + constants.GROUPID + constants.LOCALUSER.CompanyId + '/' + this.state.groupId);

    fetch(constants.MAINURL + constants.GROUPID + constants.LOCALUSER.CompanyId + '/' + this.state.groupId)
      .then(response => response.json())
      .then((responseJson) => {
        console.log('groupData Response--->', responseJson);
        if (responseJson.Status === 'Unsuccessful') {
          this.setState({
            groupData: [],
            isLoading: false
          })
        } else {
          this.setState({
            groupData: responseJson.GetLastLocationOfAllUsersOfAnyGroupResult
          }, () => this.setState({ isLoading: false }))
        }
      })
      .catch(error => console.log(error))
  }

  renderItem(item, index) {
    return (
      <TouchableOpacity onPress={() => this.props.navigation.navigate('UserMaps', { userData: item })} style={style.flatListView} >
        <View style={style.view1} >
          <View style={{ marginLeft: 5 }} >
            <Image style={style.imageList} source={{ uri: item.UserPhoto }} />
          </View>
          <View style={style.textView} >
            <Text style={{ color: 'orange' }} >{item.username}</Text>
            <Text numberOfLines={2} style={{ marginRight: 10 }} >{item.LocationArea}</Text>
          </View>
        </View>
      </TouchableOpacity >
    )
  }
  render() {
    return (
      <View style={{ flex: 1, }}>
        {
          (this.state.isLoading) ? <ActivityIndicator size="large" style={style.activityView} /> :
            (this.state.groupData.length > 0) ?
              <View style={{ flex: 1 }} >
                <View style={{ height: screenHeight * 0.6 }}>
                  <MapView
                    loadingEnabled
                    style={{ height: screenHeight * 0.6 }}
                    region={{
                      latitude: Number(this.state.groupData[0].Latitude),
                      longitude: Number(this.state.groupData[0].Longitude),
                      latitudeDelta: 0.1,
                      longitudeDelta: 0.1,
                    }}
                  >
                    {
                      this.state.groupData.map((item, index) => {
                        return (
                          <Marker
                            key={index}
                            coordinate={{
                              latitude: Number(item.Latitude),
                              longitude: Number(item.Longitude),
                            }}
                            title={item.LocationArea}
                          >
                            <View key={index} style={{ alignItems: 'center' }} >
                              <Image style={style.markerView} source={require('../../assets/images/iconMap.png')} />
                              <Image style={style.markerImage} source={{ uri: item.UserPhoto }} />
                            </View>
                          </Marker>
                        )
                      })
                    }
                  </MapView>
                </View>
                <View style={{ flex: 1 }} >
                  <FlatList
                    showsVerticalScrollIndicator={false}
                    style={{ marginTop: 10, marginHorizontal: 5, }}
                    data={this.state.groupData}
                    renderItem={({ item, index }) => this.renderItem(item, index)}
                    extraData={this.state}
                    keyExtractor={(item, index) => index.toString()}
                    ListFooterComponent={() => <View style={{ height: screenHeight * 0.1, }} />}
                  />
                </View>

              </View>
              :
              <View style={style.emptyView} >
                <Text style={{ fontSize: constants.FONT_16 }} >Recently no user in the group</Text>
                <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={style.goBack} >
                  <Text numberOfLines={2} style={{ color: 'white' }} >Go back</Text>
                </TouchableOpacity>
              </View>

        }
      </View>
    );
  }
}



export default GroupMenu;