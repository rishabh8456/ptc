import {
  Platform,
  StyleSheet,
  Dimensions
} from "react-native";
import * as constants from '../../common/constant';

const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width

module.exports = StyleSheet.create({

  activityView: {
    marginTop: screenHeight * 0.5, alignSelf: 'center', justifyContent: 'center'
  },
  markerView: {
    position: 'absolute', resizeMode: "contain", height: 50, width: 50
  },
  markerImage: {
    alignSelf: 'center', marginTop: 3, justifyContent: 'center', borderRadius: 20, height: 30, width: 30, resizeMode: "contain"
  },
  emptyView: {
    alignItems: 'center', justifyContent: "center", marginTop: screenHeight / 2.5
  },
  goBack: {
    marginTop: 10, borderRadius: 5, paddingHorizontal: 15, paddingVertical: 5, backgroundColor: 'orange'
  },
  flatListView: {
    borderRadius: 3, borderWidth: 0.5, marginHorizontal: 5, marginVertical: 5, paddingVertical: 5, paddingHorizontal: 5
  },
  view1: {
    flexDirection: 'row', alignItems: 'center'
  },
  imageList: {
    width: screenWidth * 0.13, height: screenWidth * 0.13, borderRadius: (screenWidth * 0.13) / 2
  },
  textView: {
    marginLeft: 5, marginRight: 5, flex: 1
  }

});
