import {
  Platform,
  StyleSheet,
  Dimensions
} from "react-native";
import * as constants from '../../common/constant';

const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width

module.exports = StyleSheet.create({
  mainView: {
    flex: 1, backgroundColor: "white", alignItems: 'center'
  },
  imageStyle: {
    marginTop: screenHeight * 0.3, height: screenWidth * 0.35, width: screenWidth * 0.35, borderRadius: (screenWidth * 0.35) / 2,
  },
  touchView: {
    marginTop: screenHeight * 0.05, alignItems: 'center', justifyContent: 'center', paddingHorizontal: 25, paddingVertical: 5, backgroundColor: 'orange', borderRadius: 10
  },
  uploadText: {
    color: 'white', fontWeight: 'bold'
  }


});
