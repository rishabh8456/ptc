import React, { Component } from 'react';
import {
  View,
  Text,
  AsyncStorage,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
  StyleSheet,
  FlatList,
  ActivityIndicator,
  Platform,
  DeviceEventEmitter,
} from 'react-native'
import * as constants from '../../common/constant';
const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width
import ImagePicker from 'react-native-image-picker';

const options = {
  title: 'Select Avatar',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};
import style from './style';


class ChangeProfile extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Change Profile Photo',
      headerTitleStyle: {
        color: 'white',
      },
      headerRight: (
        <View style={{ flex: 1 }} />
      ),
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.goBack()} style={{ marginLeft: 10, marginBottom: 5 }}>
          <Image style={{ height: screenHeight * 0.03, width: screenWidth * 0.06 }} source={require('../../assets/images/reply.png')} />
        </TouchableOpacity>
      ),
      headerStyle: {
        backgroundColor: 'rgb(0,100,89)',
      },
    };
  }


  state = {
    avatarSource: this.props.navigation.state.params.data,
    avatarData: null
  }

  componentDidMount() {
    DeviceEventEmitter.addListener('RNUploaderProgress', (data) => {
      let bytesWritten = data.totalBytesWritten;
      let bytesTotal = data.totalBytesExpectedToWrite;
      let progress = data.progress;

      console.log("upload progress: " + data.progress + "%");
    });
  }

  launchImage() {
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          avatarSource: response.uri,
          avatarData: response
        });
      }
    });
  }

  // createFormData = (photo) => {
  //   const data = new FormData();

  //   data.append("photo", {
  //     name: photo.fileName,
  //     type: photo.type,
  //     uri:
  //       Platform.OS === "android" ? photo.uri : photo.uri
  //   });
  //   return data;
  // };

  updateProfile() {
    console.log(this.state.avatarData);

    if (this.state.avatarData !== null) {

      const ImageData = new FormData();

      ImageData.append('photo', {
        uri: this.state.avatarData.uri,
        type: this.state.avatarData.type,
        name: this.state.avatarData.fileName
      });



      //   fetch('http://apiptcfamilytrack.ptcschool.in/api/ImageUpload/ImageFile', {
      //     method: 'POST',
      //     body: ImageData,
      //   })
      //     .then(
      //       response => response, // if the response is a JSON object
      //     )
      //     .then(
      //       success => console.log('success: ', success), // Handle the success response object
      //     )
      //     .catch(
      //       error => console.log('Error: ', error), // Handle the error response object
      //     );
      // };

      fetch("http://apiptcfamilytrack.ptcschool.in/api/ImageUpload/ImageFile", {
        method: "POST",
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        body: ImageData
      })
        .then(response => response.json())
        .then(response => {
          console.log("upload succes", response);
          fetch(constants.MAINURL + constants.USERIMAGECHANGE + constants.LOCALUSER.CompanyId + '/' + constants.LOCALUSER.UserId + '/' + this.state.avatarData.fileName + '/' + constants.LOCALUSER.UserId)
            .then(response => response.json())
            .then((responseJson) => {
              alert(JSON.stringify(responseJson))
            })
            .catch(error => console.log(error))
        }).catch(err => {
          console.log(err)

        })
        .catch(error => {
          console.log("upload error", error);
          alert("Upload failed!");
        });

      // formdata.append('ImageUpload', {uri: this.state.avatarData.uri, name: this.state.avatarData.fileName, type: this.state.avatarData.type })
      // fetch('http://apiptcfamilytrack.ptcschool.in/api/ImageUpload/ImageFile', {
      //   method: 'post',
      //   headers: {
      //     'Content-Type': 'multipart/form-data',
      //   },
      //   body: ImageData
      // }).then(response => {
      //   console.log("image uploaded", response)
      //   // fetch(constants.MAINURL + constants.USERIMAGECHANGE + constants.LOCALUSER.CompanyId + '/' + constants.LOCALUSER.UserId + '/' + this.state.avatarData.fileName + '/' + constants.LOCALUSER.UserId)
      //   //   .then(response => response.json())
      //   //   .then((responseJson) => {
      //   //     alert(JSON.stringify(responseJson))
      //   //   })
      //   //   .catch(error => console.log(error))
      // }).catch(err => {
      //   console.log(err)
      // })
    } else {
      alert('Please Select Image')
    }
  }

  render() {
    return (
      <View style={style.mainView}>
        <TouchableOpacity onPress={() => this.launchImage()} >
          <Image style={style.imageStyle} source={{ uri: this.state.avatarSource }} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this.updateProfile()} style={style.touchView} >
          <Text style={style.uploadText} >Upload</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default ChangeProfile;