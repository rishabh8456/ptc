import React, { Component } from 'react';

import {
  View,
  Platform,
  StyleSheet,
  Dimensions,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
  SafeAreaView,
  TextInput,
  AsyncStorage,
} from 'react-native'

import * as constants from '../../common/constant';
import style from './style';

const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width

class LogIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userEmail: '9355522262',
      userPassword: 'anil.ptc'
    }
  }


  userLogin() {
    console.log(constants.MAINURL + constants.LOGIN + this.state.userEmail + "?" + constants.USERPASSWORD + this.state.userPassword);

    fetch(constants.MAINURL + constants.LOGIN + this.state.userEmail + "/?" + constants.USERPASSWORD + this.state.userPassword)
      .then(response => response.json())
      .then((responseJson) => {
        console.log('Login Response--->', responseJson);
        if (responseJson.VerifyUserMobileNumberBasedResult.Status === 'Unsuccessful') {
          alert('Invalid Username or Password')
        } else {
          AsyncStorage.setItem(constants.ISLOGIN, 'true');
          AsyncStorage.setItem(constants.USERDATA, JSON.stringify(responseJson.VerifyUserMobileNumberBasedResult))
          this.props.navigation.navigate('HomeScreen', { data: responseJson.VerifyUserMobileNumberBasedResult })
        }

      })
      .catch(error => alert(error))
  }

  render() {
    return (
      <SafeAreaView style={style.MainContainer}>
        <ImageBackground style={{ flex: 1 }} source={require('../../assets/images/new_login_screen.png')} >
          <Image style={style.logoImage} source={require('../../assets/images/ptclogo.png')} />
          <Image style={style.logoText} source={require('../../assets/images/ptcnamelogo.png')} />
          <View style={style.view1} >
            <View style={style.view2} >
              <View style={style.buttonView} >
                <Image style={style.imageStyle} source={{ uri: "https://img.icons8.com/ios/64/000000/user-filled.png" }} />
                <TextInput
                  keyboardType={'number-pad'}
                  value={this.state.userEmail}
                  maxLength={10}
                  style={style.textInputText}
                  onChangeText={(text) => this.setState({ userEmail: text })}
                />
              </View>
              <View style={[style.buttonView, { marginTop: 10 }]} >
                <Image style={style.imageStyle} source={{
                  uri: "https://img.icons8.com/material-two-tone/24/000000/password1.png"
                }} />
                <TextInput
                  style={style.textInputText}
                  value={this.state.userPassword}
                  secureTextEntry={true}
                  onChangeText={(text) => this.setState({ userPassword: text })}
                />
              </View>
              <View style={style.view3} >
                <Text style={{ color: 'darkgreen' }} >How To Use?</Text>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('OtpScreen')} >
                  <Text style={{ color: 'darkgreen' }}>Forgot Password?</Text>
                </TouchableOpacity>
              </View>
              <View style={style.view4} >
                <TouchableOpacity onPress={() => this.userLogin()} style={style.loginTouch} >
                  <Text style={style.loginText} >Login</Text>
                </TouchableOpacity>
                <View style={style.view5} >
                  <Text style={style.orTextstyle}>Or</Text>
                </View>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Registration')} style={style.registrationTouch} >
                  <Text style={style.registrationText} >How User..?  Registration </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}

export default LogIn;

