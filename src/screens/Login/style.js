import {
  Platform,
  StyleSheet,
  Dimensions
} from "react-native";
import * as constants from '../../common/constant';

const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width

module.exports = StyleSheet.create({
  MainContainer: {
    flex: 1,
    backgroundColor: 'white'
  },
  logoImage: {
    marginTop: screenHeight * 0.08, resizeMode: 'contain', height: screenHeight * 0.15, alignSelf: "center"
  },
  logoText: {
    marginTop: 10, resizeMode: 'contain', height: screenHeight * 0.05, alignSelf: "center"
  },
  view1: {
    backgroundColor: 'transparent', flex: 1
  },
  view2: {
    backgroundColor: 'transparent', marginTop: screenHeight * 0.13, width: screenWidth
  },
  imageStyle: {
    height: screenHeight * 0.05,
    width: screenWidth * 0.06,
    marginLeft: screenWidth * 0.06,
    marginRight: screenWidth * 0.1,
    resizeMode: 'contain'
  },
  buttonView: {
    borderWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 20,
    borderColor: 'black',
    height: screenHeight * 0.07,
    width: screenWidth - 30,
    backgroundColor: 'white',
    marginHorizontal: 15
  },
  textInputText: {
    textAlign: 'center', height: screenHeight * 0.05, flex: 1, marginRight: screenWidth * 0.1
  },
  view3: {
    marginHorizontal: 20, flexDirection: 'row', marginTop: 10, justifyContent: "space-between"
  },
  view4: {
    alignItems: 'center', marginTop: screenHeight * 0.05
  },
  loginTouch: {
    borderRadius: 30, height: screenHeight * 0.06, width: screenWidth * 0.55, backgroundColor: 'orange', alignItems: 'center', justifyContent: 'center'
  },
  loginText: {
    color: 'white', fontSize: screenHeight * 0.0259, fontWeight: 'bold'
  },
  view5:{
    marginVertical: screenHeight * 0.03
  },
  orTextstyle:{
    textAlign: 'center', color: 'black', fontSize: screenHeight * 0.0194, fontWeight: 'bold'
  },
  registrationTouch:{
    borderWidth: 1, borderRadius: 30, borderColor: 'orange', height: screenHeight * 0.06, width: screenWidth * 0.55, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center'
  },
  registrationText:{
    textAlign: 'center', color: 'darkgreen', fontSize: screenHeight * 0.0194, fontWeight: 'normal'
  }
});
