import {
  Platform,
  StyleSheet,
  Dimensions
} from "react-native";
import * as constants from '../../common/constant';

const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width

module.exports = StyleSheet.create({
  mainView: {
    flex: 1, backgroundColor: 'white'
  },
  view1: {
    height: screenHeight * 0.25, alignItems: 'center', justifyContent: 'center', backgroundColor: 'yellow'
  },
  image1: {
    resizeMode: 'contain', height: screenWidth * 0.2, width: screenWidth * 0.2
  },
  successText: {
    marginTop: 15, color: 'black', fontWeight: "bold", fontSize: constants.FONT_16
  },
  view2: {
    flex: 1, backgroundColor: "rgb(228,228,215)"
  },
  view3: {
    alignItems: 'center', flexDirection: 'row', width: screenWidth, borderTopWidth: 2, borderColor: 'white', paddingVertical: 20
  },
  headerText: {
    fontSize: constants.FONT_12, marginLeft: 20, color: 'rgb(0,100,89)'
  },
  detailsText: {
    marginRight: 10, width: screenWidth - 100, fontSize: constants.FONT_12, marginLeft: 10, color: 'rgb(0,100,89)'
  },
  detailText1: {
    fontSize: constants.FONT_12, marginRight: 10, width: screenWidth - 100, marginLeft: 10, color: 'rgb(0,100,89)'
  },
  detailsText2: {
    fontSize: constants.FONT_12, marginLeft: 10, color: 'rgb(0,100,89)'
  }
});
