import React, { Component } from 'react';
import {
  View,
  Text,
  AsyncStorage,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
  StyleSheet,
  FlatList,
  ActivityIndicator,
  ImageBackground
} from 'react-native'
import * as constants from '../../common/constant';
const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width
import style from './style';

class SuccessRecharge extends Component {


  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Txn Status',
      headerTitleStyle: {
        color: 'white',
      },
      headerRight: (
        <View style={{ flex: 1 }} />
      ),
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.goBack()} style={{ marginLeft: 10, marginBottom: 5 }}>
          <Image style={{ height: screenHeight * 0.03, width: screenWidth * 0.06 }} source={require('../../assets/images/reply.png')} />
        </TouchableOpacity>
      ),
      headerStyle: {
        backgroundColor: 'rgb(0,100,89)',
      },
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      rechargeDetails: this.props.navigation.state.params.rechargeData
    }
  }
  render() {

    const { rechargeDetails } = this.state

    return (
      <View style={style.mainView} >
        <ImageBackground style={style.view1}  >
          <Image style={style.image1} source={require('../../assets/images/tc_donn.png')} />
          <Text style={style.successText} >Recharge Successfully</Text>
        </ImageBackground>
        <View style={style.view2} >
          <View style={style.view3} >
            <Text style={style.headerText} >Txn Id :</Text>
            <Text numberOfLines={2} style={style.detailsText} >{rechargeDetails.TXNID}</Text>
          </View>
          <View style={style.view3} >
            <Text style={style.headerText} >Order Id :</Text>
            <Text numberOfLines={2} style={style.detailText1} >{rechargeDetails.ORDERID}</Text>
          </View>
          <View style={style.view3} >
            <Text style={style.headerText} >Date :</Text>
            <Text style={style.detailsText2} >{rechargeDetails.TXNDATE}</Text>
          </View>
          <View style={style.view3} >
            <Text style={style.headerText} >Amount :</Text>
            <Text style={style.detailsText2} >{rechargeDetails.TXNAMOUNT + ' Rupees'}</Text>
          </View>
        </View>
      </View>
    );
  }
}

export default SuccessRecharge;