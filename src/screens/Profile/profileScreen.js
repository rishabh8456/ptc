import React, { Component } from 'react';
import {
  View,
  Text,
  AsyncStorage,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
  StyleSheet,

} from 'react-native'
import * as constants from '../../common/constant';
const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width
import style from './style'

class ProfileScreen extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Profile',
      headerTitleStyle: {
        color: 'white',
      },
      headerRight: (
        <View style={{ flex: 1 }} />
      ),
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.goBack()} style={{ marginLeft: 10, marginBottom: 5 }}>
          <Image style={{ height: screenHeight * 0.03, width: screenWidth * 0.06 }} source={require('../../assets/images/reply.png')} />
        </TouchableOpacity>
      ),
      headerStyle: {
        backgroundColor: 'rgb(0,100,89)',
      },
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      userData: [],
      oldPassword: '',
      newPassword: '',
      paytmSaveData: [],
      paytmNumber: '',
      paytmName: '',
      bankSaveData: [],
      accountNumber: '',
      IFSCCode: '',
      bankName: '',
      branchName: '',
      accountHolderName: '',
      isPasswordCheck: false,
      isPaytmCheck: false,
      isBankCheck: false
    }
  }

  changePassword() {
    console.log(constants.MAINURL + constants.CHANGEPASSWORD + '/' + constants.LOCALUSER.CompanyId + '/' + constants.LOCALUSER.UserId + '/' + this.state.oldPassword + '?NewPassword=' + this.state.newPassword);

    fetch(constants.MAINURL + constants.CHANGEPASSWORD + '/' + constants.LOCALUSER.CompanyId + '/' + constants.LOCALUSER.UserId + '/' + this.state.oldPassword + '?NewPassword=' + this.state.newPassword)
      .then(response => response.json())
      .then((responseJson) => {
        console.log('ChangeResponse--->', responseJson);
        alert(responseJson.Status)
        this.setState({
          isPaytmCheck: false,
          isBankCheck: false,
          isPasswordCheck: false
        })
      })
      .catch(error => alert(error))
  }

  paytmSaveData() {
    console.log(constants.MAINURL + constants.PAYTMSAVEDETAILS + constants.LOCALUSER.CompanyId + '/' + constants.LOCALUSER.UserId);

    fetch(constants.MAINURL + constants.PAYTMSAVEDETAILS + constants.LOCALUSER.CompanyId + '/' + constants.LOCALUSER.UserId)
      .then(response => response.json())
      .then((responseJson) => {
        console.log('PAYTMSAVEDETAILS--->', responseJson.GetSavedPaytmDetailsResult);
        this.setState({
          paytmSaveData: responseJson.GetSavedPaytmDetailsResult
        }, () => {
          this.setState({
            paytmName: 'Name: ' + this.state.paytmSaveData.PaytmHolderName,
            paytmNumber: 'Paytm No: ' + this.state.paytmSaveData.PayTmNumber
          })
        })
      })
      .catch(error => console.log(error))
  }

  paytmUpdate() {
    console.log(constants.MAINURL + constants.PAYTMUPDATEDETAILS + constants.LOCALUSER.CompanyId + '/' + constants.LOCALUSER.UserId + '/' + this.state.paytmNumber + '/?' + 'PayTmAccountHolderName=' + this.state.paytmName);

    fetch(constants.MAINURL + constants.PAYTMUPDATEDETAILS + constants.LOCALUSER.CompanyId + '/' + constants.LOCALUSER.UserId + '/' + this.state.paytmNumber + '/?' + 'PayTmAccountHolderName=' + this.state.paytmName)
      .then(response => response.json())
      .then((responseJson) => {
        alert(responseJson.Status)
        this.setState({
          isPaytmCheck: false,
          isBankCheck: false,
          isPasswordCheck: false
        })
      })
      .catch(error => console.log(error))
  }

  bankSaveData() {
    fetch(constants.MAINURL + constants.BANKSAVEDETAILS + constants.LOCALUSER.CompanyId + '/' + constants.LOCALUSER.UserId + '/Account')
      .then(response => response.json())
      .then((responseJson) => {
        console.log('bankSaveData--->', responseJson.GetSavedAccountOrPaytmDetailsResult);
        this.setState({
          bankSaveData: responseJson.GetSavedAccountOrPaytmDetailsResult
        }, () => {
          this.setState({
            accountHolderName: this.state.bankSaveData.AccountHolderName,
            accountNumber: this.state.bankSaveData.AccountNumber,
            IFSCCode: this.state.bankSaveData.IFSCcode,
            bankName: this.state.bankSaveData.BankName,
            branchName: this.state.bankSaveData.BranchName
          })

        })
      })
      .catch(error => console.log(error))
  }

  bankUpdateData() {
    console.log(constants.MAINURL + constants.BANKUPDATEDETAILS + constants.LOCALUSER.CompanyId + '/' + constants.LOCALUSER.UserId + '/' + this.state.accountNumber + '/' + this.state.IFSCCode + '/' + this.state.bankName + '/' + this.state.branchName + '/?' + 'AccountHolderName=' + this.state.accountHolderName);

    fetch(constants.MAINURL + constants.BANKUPDATEDETAILS + constants.LOCALUSER.CompanyId + '/' + constants.LOCALUSER.UserId + '/' + this.state.accountNumber + '/' + this.state.IFSCCode + '/' + this.state.bankName + '/' + this.state.branchName + '/?' + 'AccountHolderName=' + this.state.accountHolderName)
      .then(response => response.json())
      .then((responseJson) => {
        alert(responseJson.Status)
        this.setState({
          isPaytmCheck: false,
          isBankCheck: false,
          isPasswordCheck: false
        })
      })
      .catch(error => console.log(error))
  }

  async componentDidMount() {
    console.log(constants.MAINURL + constants.USERDETAILS + constants.LOCALUSER.CompanyId + '/' + constants.LOCALUSER.UserId);

    fetch(constants.MAINURL + constants.USERDETAILS + constants.LOCALUSER.CompanyId + '/' + constants.LOCALUSER.UserId)
      .then(response => response.json())
      .then((responseJson) => {
        console.log('Login Response--->', responseJson);
        this.setState({
          userData: responseJson.GetUserProfileWithAccountDetailsResult
        })

      })
      .catch(error => console.log(error))

    this.paytmSaveData()
    this.bankSaveData()
  }


  render() {
    return (
      <View style={style.mainView}>
        <View style={style.view1} >
          <View style={style.imageView} >
            <Image style={style.imageStyle} source={{ uri: this.state.userData.UserPhoto }} />
          </View>
          <View style={style.view2} >
            <Text style={style.userName} >{this.state.userData.UserName}</Text>
          </View>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('ChangeProfile', { data: this.state.userData.UserPhoto })} style={style.changeImageTouch} >
            <Image style={{ height: 20, width: 20 }} source={require('../../assets/images/edittttt.png')} />
          </TouchableOpacity>
        </View>
        <View style={style.view3} >
          <View style={style.subView1} >
            <Text style={style.headerText} >Mobile :</Text>
            <Text style={style.headerText1} >{this.state.userData.UserMobile}</Text>
          </View>
          <View style={style.subView2} >
            <Text style={style.headerText} >Reffral code :</Text>
            <Text style={style.headerText1} >{this.state.userData.AdminReffralCode}</Text>
          </View>
          <View style={style.subView2} >
            <Text style={style.headerText} >Recharge Expire :</Text>
            <Text style={style.headerText1} >{this.state.userData.RechargeExpiryDate}</Text>
          </View>
          <View style={style.subView2} >
            <TouchableOpacity style={{ marginLeft: screenWidth * 0.05 }} onPress={() => this.setState({ isPasswordCheck: !this.state.isPasswordCheck, isBankCheck: false, isPaytmCheck: false })}>
              {
                (!this.state.isPasswordCheck) ?
                  <Image style={{ height: 20, width: 20 }} source={{ uri: 'https://img.icons8.com/ios/50/000000/unchecked-checkbox.png' }} />
                  :
                  <Image style={{ height: 20, width: 20 }} source={{ uri: 'https://img.icons8.com/metro/26/000000/checked-2.png' }} />
              }
            </TouchableOpacity>
            <Text style={{ marginLeft: 10, color: 'rgb(0,100,89)' }} >Change Password</Text>

          </View>
          {
            (this.state.isPasswordCheck) ?
              <View style={style.passwordView} >
                <TextInput onChangeText={(text) => this.setState({ oldPassword: text })} value={this.state.oldPassword} placeholder={'Enter old Password'} style={[style.passwordText, { marginTop: screenHeight * 0.01, }]} />
                <TextInput onChangeText={(text) => this.setState({ newPassword: text })} value={this.state.newPassword} placeholder={'Enter new Password'} style={[style.passwordText, { marginTop: screenHeight * 0.02, }]} />
                <TouchableOpacity onPress={() => this.changePassword()} style={style.passwordSave} >
                  <Text style={style.saveText} >Save</Text>
                </TouchableOpacity>
              </View>
              :
              null
          }
          <View style={style.subView2} >
            <TouchableOpacity style={{ marginLeft: screenWidth * 0.05 }} onPress={() => this.setState({ isPaytmCheck: !this.state.isPaytmCheck, isPasswordCheck: false, isBankCheck: false })}>
              {
                (!this.state.isPaytmCheck) ?
                  <Image style={{ height: 20, width: 20 }} source={{ uri: 'https://img.icons8.com/ios/50/000000/unchecked-checkbox.png' }} />
                  :
                  <Image style={{ height: 20, width: 20 }} source={{ uri: 'https://img.icons8.com/metro/26/000000/checked-2.png' }} />
              }
            </TouchableOpacity>
            <Text style={{ marginLeft: 10, color: 'rgb(0,100,89)' }} >Update Paytm Details</Text>
          </View>
          {
            (this.state.isPaytmCheck) ?
              <View style={style.passwordView} >
                <Image style={{ marginBottom: 10 }} source={require('../../assets/images/pauuutm.png')} />
                <TextInput placeholder={'Name'} onChangeText={(text) => this.setState({ paytmName: text })} value={this.state.paytmName} style={[style.passwordText, { marginTop: screenHeight * 0.01, }]} />
                <TextInput placeholder={'paytm Number'} onChangeText={(text) => this.setState({ paytmNumber: text })} maxLength={10} value={this.state.paytmNumber} style={[style.passwordText, { marginTop: screenHeight * 0.02, }]} />
                <TouchableOpacity onPress={() => this.paytmUpdate()} style={style.passwordSave} >
                  <Text style={style.saveText} >Save</Text>
                </TouchableOpacity>
              </View>
              :
              null
          }
          <View style={style.subView2} >
            <TouchableOpacity style={{ marginLeft: screenWidth * 0.05 }} onPress={() => this.setState({ isBankCheck: !this.state.isBankCheck, isPasswordCheck: false, isPaytmCheck: false })}>
              {
                (!this.state.isBankCheck) ?
                  <Image style={{ height: 20, width: 20 }} source={{ uri: 'https://img.icons8.com/ios/50/000000/unchecked-checkbox.png' }} />
                  :
                  <Image style={{ height: 20, width: 20 }} source={{ uri: 'https://img.icons8.com/metro/26/000000/checked-2.png' }} />
              }
            </TouchableOpacity>
            <Text style={{ marginLeft: 10, color: 'rgb(0,100,89)' }} >Update Bank A/C Details</Text>

          </View>
          {
            (this.state.isBankCheck) ?
              <View style={style.passwordView} >
                <Image style={{ marginBottom: 10 }} source={require('../../assets/images/bankkk.png')} />
                <TextInput placeholder={'Account holder Name'} onChangeText={(text) => this.setState({ accountHolderName: text })} value={this.state.accountHolderName} style={[style.passwordText, { marginTop: screenHeight * 0.01, }]} />

                <TextInput placeholder={'Account No.'} onChangeText={(text) => this.setState({ accountNumber: text })} maxLength={10} value={this.state.accountNumber} style={[style.passwordText, { marginTop: screenHeight * 0.02, }]} />

                <TextInput placeholder={'Ifsc code'} onChangeText={(text) => this.setState({ IFSCCode: text })} maxLength={10} value={this.state.IFSCCode} style={[style.passwordText, { marginTop: screenHeight * 0.02, }]} />

                <TextInput placeholder={'Bank Name'} onChangeText={(text) => this.setState({ bankName: text })} maxLength={10} value={this.state.bankName} style={[style.passwordText, { marginTop: screenHeight * 0.02, }]} />

                <TextInput placeholder={'Branch Name'} onChangeText={(text) => this.setState({ branchName: text })} maxLength={10} value={this.state.branchName} style={[style.passwordText, { marginTop: screenHeight * 0.02, }]} />

                <TouchableOpacity onPress={() => this.bankUpdateData()} style={style.passwordSave} >
                  <Text style={style.saveText} >Save</Text>
                </TouchableOpacity>
              </View>
              :
              null
          }
        </View>
      </View >
    );
  }
}

export default ProfileScreen;