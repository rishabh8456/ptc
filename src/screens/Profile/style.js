import {
  Platform,
  StyleSheet,
  Dimensions
} from "react-native";
import * as constants from '../../common/constant';

const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width

module.exports = StyleSheet.create({
  mainView: {
    flex: 1, backgroundColor: "rgb(228,228,215)"
  },
  view1: {
    marginTop: screenHeight * 0.02, flexDirection: 'row'
  },
  imageView: {
    marginLeft: 20, alignItems: 'center',
  },
  imageStyle: {
    height: screenWidth * 0.25, width: screenWidth * 0.25, borderRadius: (screenWidth * 0.25) / 2, resizeMode: 'contain'
  },
  view2: {
    alignItems: 'center', flex: 1, alignSelf: 'center', justifyContent: 'center'
  },
  userName: {
    textAlign: 'center', color: 'rgb(0,100,89)', fontSize: constants.FONT_14
  },
  changeImageTouch: {
    alignSelf: 'flex-start', marginRight: 10
  },
  view3: {
    marginTop: screenHeight * 0.02
  },
  subView1: {
    alignItems: 'center', flexDirection: 'row', width: screenWidth, borderTopWidth: 2, borderColor: 'white', paddingVertical: 5
  },
  headerText: {
    marginLeft: 20, color: 'rgb(0,100,89)'
  },
  headerText1: {
    marginLeft: 10, color: 'rgb(0,100,89)'
  },
  subView2: {
    alignItems: 'center', flexDirection: 'row', width: screenWidth, paddingVertical: 5, borderTopWidth: 2, borderColor: 'white'
  },
  passwordView: {
    alignSelf: 'center', paddingTop: screenHeight * 0.01, paddingBottom: screenHeight * 0.02, width: '95%', backgroundColor: 'white', alignItems: "center", borderRadius: 30, marginTop: screenHeight * 0.01, marginBottom: screenHeight * 0.01
  },
  passwordText: {
    textAlign: 'center', width: '90%', borderBottomWidth: 2, borderBottomColor: 'red'
  },
  passwordSave: {
    borderRadius: 5, paddingHorizontal: screenWidth * 0.05, paddingVertical: 5, marginTop: screenHeight * 0.03, alignItems: "center", justifyContent: "center", backgroundColor: 'rgb(44,198,45)'
  },
  saveText: {
    color: 'white', fontWeight: 'bold', textAlign: "center"
  }

});
