import React, { Component } from 'react';
import {
  View,
  Text,
  AsyncStorage,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput
} from 'react-native'
import * as constants from '../../common/constant';
import style from './style';
const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width


class JoinGroup extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Join and Create Groups',
      headerTitleStyle: {
        color: 'white',
      },
      headerRight: (
        <View style={{ flex: 1 }} />
      ),
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.goBack()} style={{ marginLeft: 10, marginBottom: 5 }}>
          <Image style={{ height: screenHeight * 0.03, width: screenWidth * 0.06 }} source={require('../../assets/images/reply.png')} />
        </TouchableOpacity>
      ),
      headerStyle: {
        backgroundColor: 'rgb(0,100,89)',
      },
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      code: ''
    }
  }

  joinGroup() {
    console.log(constants.MAINURL + constants.JOINGROUP + constants.LOCALUSER.CompanyId + '/' + constants.LOCALUSER.UserId + '/' + this.state.code);

    fetch(constants.MAINURL + constants.JOINGROUP + constants.LOCALUSER.CompanyId + '/' + constants.LOCALUSER.UserId + '/' + this.state.code)
      .then(response => response.json())
      .then((responseJson) => {
        console.log('Join Response--->', responseJson);
        alert(responseJson.Status)

      })
      .catch(error => console.log(error))
  }

  render() {
    return (
      <View style={style.mainView} >
        <View style={style.view1}>
          <View style={style.view2} >
            <Text style={style.headerText} >Joining a Group...?</Text>
            <Text style={style.headerText}>enter a 6 Digits Group code</Text>
          </View>
          <View style={style.view3} >
            <TextInput value={this.state.code} onChangeText={(txt) => this.setState({ code: txt })} keyboardType={'number-pad'} maxLength={6} style={style.textInputView} />
          </View>
          <View style={style.view4} >
            <Image style={style.imageView} source={require('../../assets/images/group.png')} />
            <Text style={style.textData} >Get the code from the person created up your group.</Text>
          </View>
          <View style={style.view5} >
            <TouchableOpacity onPress={() => this.joinGroup()} style={style.touch1} >
              <Text style={style.whiteText} >Submit</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={style.view6}>
          <View style={style.view7} >
            <Text style={style.accountText} >Don't have a code...?</Text>
          </View>
          <View style={style.view8} >
            <TouchableOpacity onPress={() => this.props.navigation.navigate('NewGroup')} style={style.touch2} >
              <Text style={style.whiteText} >Create a new group</Text>
            </TouchableOpacity>
          </View>
          <View style={style.view9}>
            <Text style={style.shareText} >We'll give you code to share</Text>
          </View>
        </View>
      </View >
    );
  }
}

export default JoinGroup;