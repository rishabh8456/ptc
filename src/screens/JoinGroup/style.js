import {
  Platform,
  StyleSheet,
  Dimensions
} from "react-native";
import * as constants from '../../common/constant';

const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width

module.exports = StyleSheet.create({
  mainView: {
    flex: 1, backgroundColor: "white"
  },
  view1: {
    flex: 1, backgroundColor: "rgb(228,228,215)"
  },
  view2: {
    marginTop: screenHeight * 0.07, alignItems: "center"
  },
  headerText: {
    textAlign: 'center', fontSize: constants.FONT_16
  },
  view3: {
    alignSelf: 'center', marginTop: screenHeight * 0.04
  },
  textInputView: {
    textAlign: 'center', borderWidth: 1, borderColor: 'black', width: screenWidth * 0.3, height: screenHeight * 0.04, backgroundColor: 'white', borderRadius: 20
  },
  view4: {
    alignItems: 'center', marginHorizontal: screenWidth * 0.1, alignSelf: 'center', marginTop: screenHeight * 0.04, flexDirection: 'row'
  },
  imageView: {
    height: screenHeight * 0.03, width: screenWidth * 0.1
  },
  textData: {
    textAlign: 'center', marginLeft: 10, fontSize: constants.FONT_14
  },
  view5: {
    alignSelf: 'center', marginTop: screenHeight * 0.03
  },
  touch1: {
    justifyContent: 'center', alignItems: 'center', height: screenHeight * 0.04, width: screenWidth * 0.4, backgroundColor: 'orange'
  },
  view6: {
    flex: 1, backgroundColor: "white"
  },
  view7: {
    marginTop: screenHeight * 0.02, alignItems: "center"
  },
  accountText: {
    fontSize: constants.FONT_20, color: 'darkgreen', textAlign: 'center'
  },
  view8: {
    marginTop: screenHeight * 0.1, alignItems: "center"
  },
  touch2: {
    justifyContent: 'center', alignItems: 'center', height: screenHeight * 0.05, width: screenWidth * 0.65, backgroundColor: 'orange'
  },
  view9: {
    marginTop: screenHeight * 0.015, alignItems: "center"
  },
  shareText: {
    fontSize: constants.FONT_10, color: 'darkgreen', textAlign: 'center'
  },
  whiteText: {
    fontSize: constants.FONT_14, color: 'white'
  }
});
