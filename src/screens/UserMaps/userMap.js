import React, { Component, Fragment } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  Image,
  SafeAreaView,
} from "react-native";
import MapView, {
  Marker,
  AnimatedRegion,
  Polyline,
} from "react-native-maps";
import Modal from "react-native-modal";
import * as constants from '../../common/constant';
const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width
const ASPECT_RATIO = screenWidth / screenHeight;
const LATITUDE = 0;
const LONGITUDE = 0;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
class UserMaps extends React.Component {

  static navigationOptions = ({ navigation }) => {

    const { userData } = navigation.state.params

    return {
      title: userData.username,
      headerTitleStyle: {
        color: 'white',
      },
      headerRight: (
        <View style={{ flex: 1 }} />
      ),
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.goBack()} style={{ marginLeft: 10, marginBottom: 5 }}>
          <Image style={{ height: screenHeight * 0.03, width: screenWidth * 0.06 }} source={require('../../assets/images/reply.png')} />
        </TouchableOpacity>
      ),
      headerStyle: {
        backgroundColor: 'rgb(0,100,89)',
      },
    };
  }

  constructor(props) {
    super(props);

    this.state = {
      latitude: LATITUDE,
      longitude: LONGITUDE,
      routeCoordinates: [],
      distanceTravelled: 0,
      prevLatLng: {},
      coordinate: new AnimatedRegion({
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      }),
      initialRegion: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      currentUserData: {},
      isLoading: true,
      userData: this.props.navigation.state.params.userData,
      isProfileVisible: false
    };
  }



  componentDidMount() {
    const { userData } = this.props.navigation.state.params;
    const { coordinate, } = this.state;
    this.props.navigation.setParams({
      userName: userData.username
    })

    console.log("userData-->", userData);
    this.setState({
      latitude: Number(userData.Latitude),
      longitude: Number(userData.Longitude),
      initialRegion: {
        latitude: Number(userData.Latitude),
        longitude: Number(userData.Longitude),
        latitudeDelta: 0.01,
        longitudeDelta: 0.01,
      }
    })

    this.countDownTimer()
  }

  profileModal = () => {
    this.setState({ isProfileVisible: !this.state.isProfileVisible });
  };

  countDownTimer() {
    setInterval(() => {
      this.TrackUser()
    }, 1000);
  }

  TrackUser() {
    const { userData } = this.props.navigation.state.params;

    fetch(constants.MAINURL + constants.CURRENTLOC + userData.CompanyId + "/" + userData.UserId)
      .then(response => response.json())
      .then((responseJson) => {
        this.setState({
          isLoading: false,
          currentUserData: responseJson.GetLastLocationOfUserResult,
          latitude: Number(responseJson.GetLastLocationOfUserResult.Latitude),
          longitude: Number(responseJson.GetLastLocationOfUserResult.Longitude),
          coordinate: new AnimatedRegion({
            latitude: Number(responseJson.GetLastLocationOfUserResult.Latitude),
            longitude: Number(responseJson.GetLastLocationOfUserResult.Longitude),
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA
          }),


        }, () => {
          const { latitude, longitude, coordinate, routeCoordinates } = this.state;
          let newCoordinate = {
            latitude, longitude
          }
          this.setState({
            routeCoordinates: routeCoordinates.concat([newCoordinate]),
          })
          coordinate.timing(newCoordinate).start();
          this.mapView.animateCamera({
            center: {
              latitude: Number(this.state.latitude),
              longitude: Number(this.state.longitude),
            },
            pitch: 45,
            heading: 90,
            altitude: 1000
          })
        })
      })
      .catch(error => console.log('error', error))



  }

  getMapRegion = () => ({
    latitude: Number(this.state.latitude),
    longitude: Number(this.state.longitude),
  });

  sendAlert() {
    const { userData } = this.state
    console.log(constants.MAINURL + constants.SENDNOTIFICATION + userData.CompanyId + "/" + userData.UserId + '/' + 'ptcaudiofile');

    fetch(constants.MAINURL + constants.SENDNOTIFICATION + userData.CompanyId + "/" + userData.UserId + '/' + 'ptcaudiofile')
      .then(response => response.json())
      .then((responseJson) => {
        console.log('GetListOfSafeZonesForChildResult--->', responseJson);
        if (responseJson.Status !== 'Unsuccessful') {
          alert('Alert Send')
        } else {

        }
      })
      .catch(error => console.log(error))
  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: 'rgb(0,100,89)', }} ></SafeAreaView>
        <View style={styles.container}>
          {
            <MapView
              style={styles.map}
              // provider={PROVIDER_GOOGLE}
              loadingEnabled={true}
              showsBuildings={true}
              showsTraffic={true}
              initialCamera={
                {
                  center: {
                    latitude: Number(this.state.latitude),
                    longitude: Number(this.state.longitude),
                  },
                  pitch: 90,
                  heading: 110,
                  zoom: 20,
                  altitude: 1000
                }
              }
              ref={ref => (this.mapView = ref)}

            >
              <Polyline coordinates={this.state.routeCoordinates} strokeWidth={5} strokeColor={'hotpink'} />
              <Marker
                coordinate={this.state.initialRegion}
                title={this.state.userData.LocationArea}
              >
                <Image style={styles.markerView} source={require('../../assets/images/iconMap.png')} />
                <Image style={styles.markerImage} source={{ uri: this.state.currentUserData.UserPhoto }} />
              </Marker>
              <Marker.Animated
                ref={marker => {
                  this.marker = marker;
                }}
                coordinate={this.state.coordinate}
              >
                <Image style={styles.markerView} source={require('../../assets/images/iconMap.png')} />
                <Image style={styles.markerImage} source={{ uri: this.state.currentUserData.UserPhoto }} />
              </Marker.Animated>

            </MapView>

          }

          <View style={styles.buttonContainer}>
          </View>
        </View>
        <View style={styles.bottomView} >
          <TouchableOpacity onPress={() => this.props.navigation.navigate('UserHistory', { userData: this.state.userData })} style={{ alignItems: 'center' }} >
            <Image style={styles.bottomImage} source={require('../../assets/images/search.png')} />
            <Text style={{}} >History</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => { this.profileModal() }} style={{ alignItems: 'center', }} >
            <Image style={styles.bottomImage} source={require('../../assets/images/alert_bell.png')} />
            <Text >Alert</Text>

            <Modal onBackdropPress={() => this.setState({ isProfileVisible: false })} isVisible={this.state.isProfileVisible}>
              <View style={styles.modelView} >
                <TouchableOpacity onPress={() => this.setState({ isProfileVisible: false })} style={styles.modelTouch} >
                  <Image style={styles.imageView} source={require('../../assets/images/delete.png')} />
                </TouchableOpacity>
                <Image style={styles.imageView1} source={{ uri: this.state.userData.UserPhoto }} />
                <Text style={styles.text1} >{this.state.userData.username}</Text>
                <TouchableOpacity onPress={() => this.sendAlert()} style={styles.touch2} >
                  <Text style={{ color: 'white' }} >Send Alert</Text>
                </TouchableOpacity>
              </View>
            </Modal>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.props.navigation.navigate('SafeZone', { userData: this.state.userData })} style={{ alignItems: 'center' }} >
            <Image style={styles.bottomImage} source={require('../../assets/images/safezonne.png')} />
            <Text>Safezone</Text>
          </TouchableOpacity>
        </View>
      </Fragment >
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: "flex-end",
    alignItems: "center",
    flex: 1
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    flex: 1
  },
  bubble: {
    flex: 1,
    backgroundColor: "rgba(255,255,255,0.7)",
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20
  },
  latlng: {
    width: 200,
    alignItems: "stretch"
  },
  button: {
    width: 80,
    paddingHorizontal: 12,
    alignItems: "center",
    marginHorizontal: 10
  },
  buttonContainer: {
    flexDirection: "row",
    marginVertical: 20,
    backgroundColor: "transparent"
  },
  markerView: {
    position: 'absolute', resizeMode: "contain", height: 50, width: 50
  },
  markerImage: {
    marginTop: 3, marginLeft: 10, borderRadius: 20, height: 30, width: 30, resizeMode: "contain"
  },
  bottomView: {
    position: 'absolute', bottom: 0, alignSelf: 'flex-end', justifyContent: 'space-around', flexDirection: 'row', height: screenHeight * 0.1, width: '100%', backgroundColor: 'white', paddingTop: screenHeight * 0.01
  },
  bottomImage: {
    height: screenHeight * 0.05, width: screenWidth * 0.09, resizeMode: 'contain'
  },
  modelView: {
    borderRadius: 5, backgroundColor: 'white', height: screenHeight * 0.3, width: screenWidth * 0.6, alignSelf: 'center', alignItems: 'center'
  },
  modelTouch: {
    alignSelf: 'flex-end'
  },
  imageView: {
    marginTop: 10, alignSelf: "flex-end", height: 20, width: 20, marginRight: 10
  },
  imageView1: {
    marginTop: 15, height: screenHeight * 0.15, width: screenWidth * 0.38
  },
  text1: {
    marginTop: 5, fontSize: constants.FONT_12
  },
  touch2: {
    borderRadius: 15, marginTop: 10, alignItems: 'center', justifyContent: 'center', paddingVertical: 10, paddingHorizontal: 20, backgroundColor: 'orange'
  }
});

export default UserMaps;