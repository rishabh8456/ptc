import React, { Component, Fragment } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  Image,
  Platform,
  PermissionsAndroid,
  ActivityIndicator,
  SafeAreaView,
  TextInput,
  KeyboardAvoidingView,
} from "react-native";
import MapView, {
  Marker,
  AnimatedRegion,
  Polyline,
  PROVIDER_GOOGLE
} from "react-native-maps";
import * as constants from '../../common/constant';
const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width
const ASPECT_RATIO = screenWidth / screenHeight;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
import DatePicker from "react-native-datepicker";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
const GOOGLE_MAPS_APIKEY = 'AIzaSyAjPuROl_1cyst6scmjCsOnZwd3jfPp6VE';
import Slider from 'react-native-slider';

class SafeZone extends React.Component {

  static navigationOptions = ({ navigation }) => {

    const { userData } = navigation.state.params

    return {
      title: userData.username,
      headerTitleStyle: {
        color: 'white',
      },
      headerRight: (
        <TouchableOpacity onPress={() => navigation.navigate('HomeScreen')} style={{ marginRight: 10, marginBottom: 5 }}>
          <Image style={{ height: screenHeight * 0.03, width: screenWidth * 0.06 }} source={require('../../assets/images/whithome.png')} />
        </TouchableOpacity>

      ),
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.goBack()} style={{ marginLeft: 10, marginBottom: 5 }}>
          <Image style={{ height: screenHeight * 0.03, width: screenWidth * 0.06 }} source={require('../../assets/images/reply.png')} />
        </TouchableOpacity>
      ),
      headerStyle: {
        backgroundColor: 'rgb(0,100,89)',
      },
    };
  }

  constructor(props) {
    super(props);

    this.state = {
      initialRegion: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      },
      initialCoordinate: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      },
      userData: this.props.navigation.state.params.userData,
      isList: false,
      storedAddress: null,
      rangeValue: 100,
      safeZoneName: '',
      changedLatLong: {}
    };
  }

  componentDidMount() {
    this.setInitialPosition()
  }

  setInitialPosition() {
    this.setState({
      initialRegion: {
        latitude: Number(this.state.userData.Latitude),
        longitude: Number(this.state.userData.Longitude),
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      },
      initialCoordinate: {
        latitude: Number(this.state.userData.Latitude),
        longitude: Number(this.state.userData.Longitude),
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      }
    })
  }

  // getaddress(data, details) {
  //   this.setState({
  //     storedAddress: null
  //   }, () => {
  //     this.setState({
  //       storedAddress: details
  //     })
  //   })
  // }

  goToLocation(nativeLatlong) {
    this.setState({
      initialRegion: {
        latitude: Number(nativeLatlong.coordinate.latitude),
        longitude: Number(nativeLatlong.coordinate.longitude),
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      },
      initialCoordinate: {
        latitude: Number(nativeLatlong.coordinate.latitude),
        longitude: Number(nativeLatlong.coordinate.longitude),
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      }
    })
  }

  saveSafeZone() {
    const { userData, initialCoordinate, rangeValue, safeZoneName } = this.state;
    if (safeZoneName.trim().length > 0) {
      fetch(constants.MAINURL + constants.SAVESAFEZONE + userData.CompanyId + "/" + userData.UserId + '/' + initialCoordinate.latitude + '/' + initialCoordinate.longitude + '/' + rangeValue + '/?safeZoneName=' + safeZoneName)
        .then(response => response.json())
        .then((responseJson) => {
          if (responseJson.Status === 'Success') {
            alert('Successfully saved')
          } else {
            alert('Please try again later')
          }
        })
        .catch(error => console.log('error', error))
    } else {
      alert('Please enter safezone name')
    }
  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={styles.mainView} ></SafeAreaView>
        <View style={styles.container}>
          <MapView
            style={styles.map}
            // provider={PROVIDER_GOOGLE}
            showsBuildings={true}
            showsTraffic={true}
            ref={ref => (this.mapView = ref)}
            initialRegion={this.state.initialRegion}
            region={this.state.initialRegion}
            onPress={e => this.goToLocation(e.nativeEvent)}
          >

            <MapView.Circle
              center={this.state.initialCoordinate}
              radius={this.state.rangeValue}
              strokeWidth={1}
              strokeColor={'rgba(208,205,243,0.8)'}
              fillColor={'rgba(208,205,243,0.8)'}
            />
            <Marker
              coordinate={this.state.initialCoordinate}
              style={{ alignItems: 'center' }}
            >
              <Image style={styles.markerView} source={require('../../assets/images/iconMap.png')} />
              <Image style={styles.markerImage} source={{ uri: this.state.userData.UserPhoto }} />
            </Marker>
          </MapView>
          <View style={styles.bottomView} >
            <TouchableOpacity onPress={() => this.setInitialPosition()} >
              <Image
                style={styles.userImage}
                source={require('../../assets/images/userone.png')} />
            </TouchableOpacity>
          </View>

          <View style={styles.bottomView1} >
            <TouchableOpacity onPress={() => this.props.navigation.navigate('SafeZoneList', { userData: this.state.userData })}>
              <Image
                style={styles.locationImage}
                source={require('../../assets/images/add_plus.png')} />
            </TouchableOpacity>
          </View>

        </View>
        {/* <View style={{ justifyContent: 'space-evenly', alignItems: 'center', flexDirection: 'row', height: screenHeight * 0.1, width: '100%', backgroundColor: "white", }} >
          <TouchableOpacity onPress={() => this.props.navigation.navigate('SafeZoneList', { userData: this.state.userData })} style={{ marginLeft: 20 }} >
            <Image
              style={{ height: screenHeight * 0.05, width: screenWidth * 0.09, resizeMode: 'contain' }}
              source={require('../../assets/images/add_plus.png')} />
          </TouchableOpacity> */}
        {/* <GooglePlacesAutocomplete
            placeholder='Search Address'
            minLength={2}
            autoFocus={false}
            returnKeyType={'search'}
            keyboardAppearance={'light'}
            listViewDisplayed={false}
            fetchDetails={true}
            renderDescription={row => row.description}
            onPress={(data, details = null) => {
              this.getaddress(data, details)
            }}
            query={{
              key: GOOGLE_MAPS_APIKEY,
              language: 'en',
              types: '(cities)'
            }}
            styles={{
              textInputContainer: {
                width: '80%',
                alignSelf: 'center',
                backgroundColor: 'white',
                borderTopWidth: 1,
                borderBottomWidth: 1,
                paddingVertical: 0,
                borderWidth: 1,
                borderRadius: 10,

              },
              description: {
                fontWeight: 'bold'
              },
              predefinedPlacesDescription: {
                color: '#1faadb'
              },
              textInput: {
                textAlign: 'center',

              },
              listView: {
                backgroundColor: "white",
                position: 'absolute',
                width: '75%',
                marginTop: screenHeight * 0.065,
                marginLeft: screenWidth * 0.085
              }
            }}
            nearbyPlacesAPI='GooglePlacesSearch'
            GooglePlacesSearchQuery={{
              rankby: 'distance',
              type: 'cafe'
            }}
            GooglePlacesDetailsQuery={{
              fields: 'formatted_address',
            }}
            filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']}
            debounce={200}

          /> */}
        {/* <TouchableOpacity onPress={() => this.goToLocation()} style={{ marginRight: 20 }} >
          <Image
            style={{ height: screenHeight * 0.05, width: screenWidth * 0.09, resizeMode: 'contain' }} source={require('../../assets/images/search.png')}
          />
        </TouchableOpacity>
        </View> */}
        <View style={styles.mainBottomView} >
          <View style={styles.subView1} >
            <View style={styles.subView2} >
              <TextInput
                placeholder={'Enter Safe Zone Name'}
                style={styles.textInput}
                value={this.state.safeZoneName}
                onChangeText={(text) => this.setState({ safeZoneName: text })}
              />
              <View style={styles.sliderView} >
                <Slider
                  minimumValue={100}
                  maximumValue={999}
                  onValueChange={(value) => this.setState({ rangeValue: value })}
                  value={this.state.rangeValue}
                  style={{ marginLeft: 10 }}
                  step={2}
                />
                <Text style={{ textAlign: 'right' }} >{this.state.rangeValue.toFixed(0)}</Text>
              </View>

              {/* <TextInput
                placeholder={'Alert zone'}
                keyboardType={'numeric'}
                returnKeyType={'done'}
                style={{
                  textAlign: 'center',
                  borderRadius: 15,
                  width: screenWidth * 0.3,
                  height: screenHeight * 0.06,
                  borderWidth: 1,
                  backgroundColor: 'white'
                }} /> */}
            </View>
            <Text style={styles.rangeText} >Alert range should be b/w 100 - 999</Text>
          </View>
          <TouchableOpacity onPress={() => this.saveSafeZone()} style={styles.safeZoneTouch} >
            <Text style={styles.safeZoneText} >Save Safe Zone Area</Text>
          </TouchableOpacity>
        </View>
      </Fragment >
    );
  }
}

const styles = StyleSheet.create({
  mainView: {
    flex: 0, backgroundColor: 'rgb(0,100,89)'
  },
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: "flex-end",
    alignItems: "center",
    flex: 1
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    flex: 1
  },
  bubble: {
    flex: 1,
    backgroundColor: "rgba(255,255,255,0.7)",
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20
  },
  latlng: {
    width: 200,
    alignItems: "stretch"
  },
  button: {
    width: 80,
    paddingHorizontal: 12,
    alignItems: "center",
    marginHorizontal: 10
  },
  buttonContainer: {
    flexDirection: "row",
    marginVertical: 20,
    backgroundColor: "transparent"
  },

  markerView: {
    position: 'absolute', resizeMode: "contain", height: 50, width: 50
  },
  markerImage: {
    marginTop: 3, borderRadius: 20, height: 30, width: 30, resizeMode: "contain"
  },
  bottomView: {
    position: 'absolute', alignSelf: 'flex-end', bottom: screenHeight * 0.2, justifyContent: 'flex-end'
  },
  userImage: {
    right: 8, height: screenHeight * 0.05, width: screenWidth * 0.09, resizeMode: 'contain'
  },
  bottomView1: {
    position: 'absolute', alignSelf: 'flex-end', bottom: screenHeight * 0.3, justifyContent: 'flex-end'
  },
  locationImage: {
    right: 10, height: screenHeight * 0.05, width: screenWidth * 0.09, resizeMode: 'contain'
  },
  mainBottomView: {
    flex: 1, position: 'absolute', bottom: 0, alignSelf: 'flex-end', height: screenHeight * 0.19, width: '100%', backgroundColor: 'white',
  },
  subView1: {
    alignItems: 'center', justifyContent: 'center', height: screenHeight * 0.12, backgroundColor: 'rgb(218,219,202)'
  },
  subView2: {
    flexDirection: 'row', marginHorizontal: 10,
  },
  textInput: {
    textAlign: 'center',
    borderRadius: 15,
    width: screenWidth * 0.6,
    height: screenHeight * 0.06,
    borderWidth: 1,
    backgroundColor: 'white',
    marginRight: 10,
  },
  sliderView: {
    width: screenWidth * 0.3,
    height: screenHeight * 0.06,
    marginTop: 10,
  },
  rangeText: {
    marginTop: 5, color: 'green'
  },
  safeZoneTouch: {
    justifyContent: 'center', alignItems: 'center', bottom: 0, height: screenHeight * 0.07, backgroundColor: 'orange'
  },
  safeZoneText: {
    fontSize: constants.FONT_16, color: 'white'
  }
});

export default SafeZone;