import React, { Component, Fragment } from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  Dimensions,
} from 'react-native'
import * as constants from '../../common/constant';
const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width
import { WebView } from 'react-native-webview';

class Policy extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.state.params.policyName,
      headerTitleStyle: {
        color: 'white',
      },
      headerRight: (
        <View style={{ flex: 1 }} />
      ),
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.goBack()} style={{ marginLeft: 10, marginBottom: 5 }
        }>
          <Image style={{ height: screenHeight * 0.03, width: screenWidth * 0.06 }} source={require('../../assets/images/reply.png')} />
        </TouchableOpacity >
      ),
      headerStyle: {
        backgroundColor: 'rgb(0,100,89)',
      },
    };
  }

  state = {
    policyData: this.props.navigation.state.params.data,
  }

  componentDidMount() {
    this.props.navigation.setParams({
      policyName: this.state.policyData.title
    })
  }

  renderLink() {
    const { policyData } = this.state;
    if (policyData.id === 1) {
      return 'http://ptccircle.com/AboutPTCCIRCLE.aspx'
    } else if (policyData.id === 2) {
      return 'http://ptccircle.com/Termsconditions.aspx'
    } else if (policyData.id === 3) {
      return 'http://ptccircle.com/Refundcancelation.aspx'
    } else if (policyData.id === 4) {
      return 'http://ptccircle.com/Multiplepaymentcases.aspx'
    } else if (policyData.id === 5) {
      return 'http://ptccircle.com/contactaddress.aspx'
    }
  }


  render() {
    return (
      <View style={{ flex: 1, backgroundColor: 'white' }} >
        <WebView
          source={{ uri: this.renderLink() }}
          style={{ flex: 1 }}
          scalesPageToFit={true}
          javaScriptEnabled={true}
          injectedJavaScript={true}
        />
      </View>
    );
  }
}

export default Policy;