import React, { Component } from 'react';
import { View, SafeAreaView, FlatList, Text, StyleSheet, Dimensions, Image, ActivityIndicator, Modal, SectionList, TouchableOpacity } from 'react-native';
import Orientation from 'react-native-orientation-locker';
import CalendarPicker from 'react-native-calendar-picker';
import * as constants from '../../common/constant';
const screenHeight = Dimensions.get('window').width
const screenWidth = Dimensions.get('window').height

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      dataSource: [],
      totalDue: 0,
      isModalVisible: false,
      isLoading: true,
      selectedStartDate: 'Select to date',
      selectedEndDate: 'Select from date',
      startCalendar: false,
      endCalendar: false,
      isRedeemModalVisible: false,
      selection: [
        { id: '1', name: 'Bank', isSelect: false },
        { id: '2', name: 'Paytm', isSelect: false },
      ],
      selectedItem: [],
    }
    this.onStartChange = this.onStartChange.bind(this);
    this.onEndChange = this.onEndChange.bind(this);
  }

  onStartChange(date) {
    console.log(date._i);
    let data = date._i
    this.setState({
      selectedStartDate: data.year + '-' + data.month + '-' + data.day,
      startCalendar: false,
      isModalVisible: true
    });
  }

  onEndChange(date) {
    console.log(date._i);
    let data = date._i
    this.setState({
      selectedEndDate: data.year + '-' + data.month + '-' + data.day,
      endCalendar: false,
      isModalVisible: true
    });
  }

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };

  redeemModal = () => {
    this.setState({ isRedeemModalVisible: !this.state.isRedeemModalVisible });
  }


  componentDidMount() {
    Orientation.lockToLandscapeRight()
    this.refresh()
  }

  componentWillUnmount() {
    Orientation.lockToPortrait()
  }

  renderItem(item, index) {
    return (
      <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center', justifyContent: "space-around", backgroundColor: (index == 0) ? 'green' : "rgb(228,228,215)", marginBottom: 5 }} >
        <View style={styles.itemParentStyle} >
          <Text style={(index == 0) ? styles.tvSelectedStyle : styles.tvUnSelectedStyle}>{item.RechargeDate}</Text>
        </View>
        <View style={styles.itemParentStyle} >
          <Text style={(index == 0) ? styles.tvSelectedStyle : styles.tvUnSelectedStyle}>{item.username}</Text>
        </View>
        <View style={styles.itemParentStyle} >
          <Text style={(index == 0) ? styles.tvSelectedStyle : styles.tvUnSelectedStyle}>{item.UserMobile}</Text>
        </View>
        <View style={styles.itemParentStyle} >
          <Text style={(index == 0) ? styles.tvSelectedStyle : styles.tvUnSelectedStyle}>{item.RechargeAmount}</Text>
        </View>
        <View style={styles.itemParentStyle} >
          <Text style={(index == 0) ? styles.tvSelectedStyle : styles.tvUnSelectedStyle}>{item.BenifitPercentage}</Text>
        </View>
        <View style={styles.itemParentStyle} >
          <Text style={(index == 0) ? styles.tvSelectedStyle : styles.tvUnSelectedStyle}>{item.BenifitAmount}</Text>
        </View>
        <View style={styles.itemParentStyle} >
          <Text style={(index == 0) ? styles.tvSelectedStyle : styles.tvUnSelectedStyle}>{item.BenifitAmountReedemed}</Text>
        </View>
        <View style={styles.itemParentStyle} >
          <Text style={(index == 0) ? styles.tvSelectedStyle : styles.tvUnSelectedStyle}> {item.TotalDue}</Text>
        </View>
      </View >
    )
  }

  refresh() {
    fetch(constants.MAINURL + constants.REEDEMPOINTS + constants.LOCALUSER.UserId)
      .then(response => response.json())
      .then((responseJson) => {
        responseJson.GetPointsPaidUnpaidReportResult.map((item) => {
          this.setState({
            totalDue: this.state.totalDue + Number(item.TotalDue)
          })
        })
        let finalResult = responseJson.GetPointsPaidUnpaidReportResult;
        let titleData = {};
        titleData.RechargeDate = "Date";
        titleData.username = 'User Name';
        titleData.UserMobile = 'Mobile No.'
        titleData.RechargeAmount = 'R.Amount'
        titleData.BenifitPercentage = 'Share %'
        titleData.BenifitAmount = 'Total Share'
        titleData.BenifitAmountReedemed = 'Reedemed'
        titleData.TotalDue = 'Total Due'

        finalResult.unshift(titleData);
        console.log(finalResult)

        this.setState({
          dataSource: finalResult,
        }, () => {
          this.setState({ isLoading: false })
        })
      })
      .catch(error => this.setState({ isLoading: false }))
  }

  openStartDate() {
    this.setState({
      isModalVisible: false
    }, () => {
      this.setState({
        startCalendar: true
      })
    })
  }

  openEndDate() {
    this.setState({
      isModalVisible: false
    }, () => {
      this.setState({
        endCalendar: true
      })
    })
  }

  searchApi() {
    console.log(constants.MAINURL + constants.SEARCHREDEEMPOINTS + constants.LOCALUSER.CompanyId + '/' + constants.LOCALUSER.UserId + '/' + this.state.selectedStartDate + '/' + this.state.selectedEndDate);

    fetch(constants.MAINURL + constants.SEARCHREDEEMPOINTS + constants.LOCALUSER.CompanyId + '/' + constants.LOCALUSER.UserId + '/' + this.state.selectedStartDate + '/' + this.state.selectedEndDate)
      .then(response => response.json())
      .then((responseJson) => {
        console.log('Login Response--->', responseJson);
        if (responseJson.Status !== 'Unsuccessful') {
          this.setState({
            dataSource: responseJson.GetRechargePlansForChildResult
          }, () => this.setState({ isLoading: false }))
        } else {
          alert('No data')
        }

      })
      .catch(error => console.log(error))
  }

  selectionOfPay(item) {

    const dataCopy = this.state.selection;
    const indexItem = dataCopy.findIndex((itm) => itm.id == item.id);

    for (let i = 0; i < dataCopy.length; i++) {
      if (indexItem == i) {
        dataCopy[indexItem].isSelect = true;
      }
      else {
        dataCopy[i].isSelect = false;
      }
    }

    this.setState({ selection: dataCopy, selectedItem: dataCopy[indexItem] });

  }

  renderSelectionItem(item, index) {
    return (
      <TouchableOpacity onPress={() => this.selectionOfPay(item)} style={{ flexDirection: 'row', alignItems: 'center', padding: 7 }} >
        {
          (!item.isSelect) ?
            <Image style={{ height: 20, width: 20, marginRight: 20 }} source={{ uri: 'https://img.icons8.com/ios/50/000000/unchecked-circle.png' }} />
            :
            <Image style={{ height: 20, width: 20, marginRight: 20 }} source={{ uri: 'https://img.icons8.com/material-two-tone/24/000000/checked-radio-button.png' }} />
        }
        <Text style={{ fontSize: constants.FONT_16 }} >{item.name}</Text>
      </TouchableOpacity>
    )
  }

  sendPayment() {
    if (this.state.selectedItem.length > 0 || this.state.selectedItem !== null) {
      if (this.state.totalDue >= '500') {
        fetch(constants.MAINURL + constants.SENDPAYMENTMETHOD + constants.LOCALUSER.UserId + '/' + this.state.selectedItem.name)
          .then(response => response.json())
          .then((responseJson) => {
            alert('Request sent Successfully...')
          })
          .catch(error => console.log(error))
      } else {
        alert("Sorry we don't Accept your request becouse you should have atleast 500 points to Redeem..")
      }
    } else {
      alert('Please select payment')
    }
  }

  searchView() {
    return (
      <View style={{
        position: 'absolute',
        flex: 1,
        height: screenHeight * 0.6, width: screenWidth * 0.5,
        backgroundColor: 'white', alignSelf: "center", top: screenHeight / 5
      }} >
        <View style={{ flex: 1, margin: 10, borderWidth: 1, alignItems: 'center' }} >
          <TouchableOpacity onPress={() => this.setState({ isModalVisible: false })} style={{ alignSelf: 'flex-end', marginTop: 10, marginRight: 10 }} >
            <Image source={require('../../assets/images/delete.png')} style={{ height: 10, width: 10 }} />
          </TouchableOpacity>
          <Text style={{ marginTop: screenHeight * 0.03 }} >Select Dates for Report</Text>
          <TouchableOpacity onPress={() => this.openStartDate()} style={{ marginTop: screenHeight * 0.05, alignItems: 'center', justifyContent: 'center', borderWidth: 1, height: screenHeight * 0.1, width: screenWidth * 0.3 }} >
            <Text>{this.state.selectedStartDate}</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.openEndDate()} style={{ marginTop: screenHeight * 0.05, alignItems: 'center', justifyContent: 'center', borderWidth: 1, height: screenHeight * 0.1, width: screenWidth * 0.3 }} >
            <Text>{this.state.selectedEndDate}</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.searchApi()} style={{ backgroundColor: 'rgb(44,198,45)', marginTop: screenHeight * 0.05, alignItems: 'center', justifyContent: 'center', paddingVertical: 5, paddingHorizontal: screenWidth * 0.02 }} >
            <Text style={{ color: 'white' }} >Search</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  redeemModalView() {
    return (
      <View style={{
        position: 'absolute',
        flex: 1,
        height: screenHeight * 0.6, width: screenWidth * 0.5,
        backgroundColor: 'white', alignSelf: "center", top: screenHeight / 5
      }} >
        <View style={{ flex: 1, margin: 10, borderWidth: 1, alignItems: 'center' }} >
          <TouchableOpacity onPress={() => this.setState({ isRedeemModalVisible: false })} style={{ alignSelf: 'flex-end', marginTop: 10, marginRight: 10 }} >
            <Image source={require('../../assets/images/delete.png')} style={{ height: 10, width: 10 }} />
          </TouchableOpacity>
          <Text style={{ marginTop: screenHeight * 0.03 }} >Redeem Points Via...?</Text>
          <View style={{ marginTop: screenHeight * 0.07, height: screenHeight * 0.2 }} >
            <FlatList
              data={this.state.selection}
              renderItem={({ item, index }) => this.renderSelectionItem(item, index)}
              extraData={this.state}
            />
          </View>
          <TouchableOpacity onPress={() => this.sendPayment()} style={{ backgroundColor: 'rgb(44,198,45)', alignItems: 'center', justifyContent: 'center', paddingVertical: 5, paddingHorizontal: screenWidth * 0.02 }} >
            <Text style={{ color: 'white' }} >Search</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  bottomView() {
    return (
      <View style={{ height: screenHeight * 0.14, width: screenWidth, backgroundColor: 'orange', bottom: 0, justifyContent: 'flex-end', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around' }} >
        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
          <Image style={{ height: 40, width: 40 }} source={require('../../assets/images/reply.png')} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this.refresh()}>
          <Image style={{ height: 40, width: 40 }} source={require('../../assets/images/refresh-button.png')} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this.toggleModal()}>
          <Image style={{ height: 40, width: 40 }} source={require('../../assets/images/searchicon.png')} />
        </TouchableOpacity>
        <Image style={{ height: 40, width: 40 }} source={require('../../assets/images/list.png')} />
        <TouchableOpacity onPress={() => this.redeemModal()} style={{ alignItems: 'center', justifyContent: 'center', borderWidth: 2, borderColor: 'white', borderRadius: 5, width: screenWidth * 0.25, height: 40, backgroundColor: 'rgb(44,198,45)' }} >
          <Text style={{ color: 'white', fontWeight: 'bold' }} >Redeem Point</Text>
        </TouchableOpacity>
        <Text style={{ fontSize: 24, color: 'white', fontWeight: 'bold' }}  >Total Due :{' ' + this.state.totalDue}</Text>
      </View>
    )
  }

  render() {
    // if (this.state.isLandScape) {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }} >
        {(this.state.isLoading) ? <View style={{ backgroundColor: "rgb(228,228,215)", flex: 1, alignItems: 'center', justifyContent: 'center' }} ><ActivityIndicator size="large" style={{ alignSelf: 'center' }} /></View> : (this.state.dataSource.length > 0) ?
          <FlatList
            style={{ height: screenHeight, width: screenWidth, backgroundColor: "rgb(228,228,215)" }}
            data={this.state.dataSource}
            renderItem={({ item, index }) => this.renderItem(item, index)}
            extraData={this.state}
          /> :
          <View style={{ flex: 1, backgroundColor: "rgb(228,228,215)", alignItems: 'center', justifyContent: 'center' }} >
            <Image style={{ resizeMode: 'contain', height: screenHeight * 0.2, width: screenWidth * 0.7 }} source={require('../../assets/images/result_no.png')} />
          </View>
        }
        {
          (this.state.startCalendar) ?
            <View style={{
              position: 'absolute',
              flex: 1,
              height: screenHeight * 0.6, width: screenWidth * 0.5,
              backgroundColor: 'white', alignSelf: "center", top: screenHeight / 5
            }} >
              <CalendarPicker
                onDateChange={this.onStartChange}
                height={screenHeight * 0.8}
                width={screenWidth * 0.6}
              />
            </View>
            : null
        }
        {
          (this.state.endCalendar) ?
            <View style={{
              position: 'absolute',
              flex: 1,
              height: screenHeight * 0.6, width: screenWidth * 0.5,
              backgroundColor: 'white', alignSelf: "center", top: screenHeight / 5
            }} >
              <CalendarPicker
                onDateChange={this.onEndChange}
                height={screenHeight * 0.8}
                width={screenWidth * 0.6}
              />
            </View>
            : null
        }
        {
          (this.state.isModalVisible) ?
            this.searchView()
            : null
        }
        {
          (this.state.isRedeemModalVisible) ?
            this.redeemModalView()
            :
            null
        }
        {
          this.bottomView()
        }
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  itemParentStyle: {
    width: screenWidth * 12.5,
    height: screenHeight * 0.05,
    alignItems: 'center',
    justifyContent: 'center'
  },
  selectedStyle: {
    width: screenWidth * 12.5,
    height: screenHeight * 0.05,
    alignItems: 'center',
    backgroundColor: 'green',
  },
  tvSelectedStyle: {
    color: 'white',
    fontWeight: 'bold'
  },
  tvUnSelectedStyle: {
    color: 'black',
    fontWeight: 'normal'
  }
});


