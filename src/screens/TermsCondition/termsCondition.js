import React, { Component, Fragment } from 'react';
import {
  View,
  Text,
  AsyncStorage,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
  StyleSheet,
  FlatList,
  Alert,
  Platform,
  SafeAreaView,

} from 'react-native'
import * as constants from '../../common/constant';
const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width

class TermsCondition extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Policies of PTC Link',
      headerTitleStyle: {
        color: 'white',
      },
      headerRight: (
        <View style={{ flex: 1 }} />
      ),
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.navigate('LogIn')} style={{ marginLeft: 10, marginBottom: 5 }
        }>
          <Image style={{ height: screenHeight * 0.03, width: screenWidth * 0.06 }} source={require('../../assets/images/reply.png')} />
        </TouchableOpacity >
      ),
      headerStyle: {
        backgroundColor: 'rgb(0,100,89)',
      },
    };
  }

  state = {
    data: [
      { id: 1, title: 'About Us..' },
      { id: 2, title: 'Terms or Conditions..' },
      { id: 3, title: 'Refund or Cancellation..' },
      { id: 4, title: 'Multiple Payment Case..' },
      { id: 5, title: 'Contact Us..' },

    ]
  }

  renderItem(item, index) {
    return (
      <TouchableOpacity onPress={() => this.props.navigation.navigate('Policy', { data: item })} style={{ alignItems: 'center', justifyContent: 'center', marginVertical: 5, height: screenHeight * 0.13, width: '100%', backgroundColor: 'rgb(218,219,202)' }} >
        <Text style={{ fontSize: constants.FONT_16 }} >{item.title}</Text>
      </TouchableOpacity>
    )
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: 'white' }} >
        <FlatList
          style={{ marginHorizontal: 10, marginTop: 5 }}
          data={this.state.data}
          renderItem={({ item, index }) => this.renderItem(item, index)}
          extraData={this.state}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}

export default TermsCondition;