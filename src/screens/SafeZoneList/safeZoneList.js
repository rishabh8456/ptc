import React, { Component } from 'react';
import {
  View,
  Text,
  AsyncStorage,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
  StyleSheet,
  Alert,
  FlatList,
  ActivityIndicator
} from 'react-native'
import * as constants from '../../common/constant';
const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width
import styles from './style';
import Geocoder from 'react-native-geocoding';
const GOOGLE_MAPS_APIKEY = 'AIzaSyAjPuROl_1cyst6scmjCsOnZwd3jfPp6VE';

class SafeZoneList extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Safe Zones',
      headerTitleStyle: {
        color: 'white',
      },
      headerRight: (
        <View style={{ flex: 1 }} />
      ),
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.goBack()} style={{ marginLeft: 10, marginBottom: 5 }}>
          <Image style={{ height: screenHeight * 0.03, width: screenWidth * 0.06 }} source={require('../../assets/images/reply.png')} />
        </TouchableOpacity>
      ),
      headerStyle: {
        backgroundColor: 'rgb(0,100,89)',
      },
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      isLoading: true,
      userData: this.props.navigation.state.params.userData,
    }
    Geocoder.init(GOOGLE_MAPS_APIKEY);
  }

  componentDidMount() {
    const { userData } = this.state;
    fetch(constants.MAINURL + constants.SAFEZONELIST + userData.CompanyId + "/" + userData.UserId)
      .then(response => response.json())
      .then((responseJson) => {
        console.log('GetListOfSafeZonesForChildResult--->', responseJson);
        if (responseJson.Status !== 'Unsuccessful') {
          this.setState({
            dataSource: responseJson.GetListOfSafeZonesForChildResult
          }, () => this.getAddress())
        } else {
          this.setState({
            dataSource: []
          }, () => this.setState({ isLoading: false }))
        }
      })
      .catch(error => console.log(error))
  }

  deleteSafeZone(item) {
    const { userData } = this.state;
    fetch(constants.MAINURL + constants.DELETESAFEZONE + userData.CompanyId + "/" + userData.UserId + '/' + item.SafeZoneId)
      .then(response => response.json())
      .then((responseJson) => {
        console.log('GetListOfSafeZonesForChildResult--->', responseJson);
        if (responseJson.Status === 'Success') {
          alert('Successfully Deleted')
          fetch(constants.MAINURL + constants.SAFEZONELIST + userData.CompanyId + "/" + userData.UserId)
            .then(response => response.json())
            .then((responseJson) => {
              console.log('GetListOfSafeZonesForChildResult--->', responseJson);
              if (responseJson.Status !== 'Unsuccessful') {
                this.setState({
                  dataSource: responseJson.GetListOfSafeZonesForChildResult
                }, () => {
                  this.getAddress()

                })
              } else {
                this.setState({
                  dataSource: []
                }, () => this.setState({ isLoading: false }))
              }
            })
            .catch(error => console.log(error))
        } else {
          alert('Please try again later')
        }
      })
      .catch(error => console.log(error))
  }

  getAddress() {
    let data = this.state.dataSource
    data.map((item) => {
      Geocoder.from(item.Latitude, item.Longitude)
        .then(json => {
          item.address = json.results[0].formatted_address;
          this.setState({
            dataSource: data,
            isLoading: false
          }, () => console.log(this.state.dataSource))
        })
        .catch(error => console.warn(error));
    })
  }

  renderItem(item, index) {
    return (

      <View style={styles.touchView} >
        <View style={{ marginTop: 5, flexDirection: 'row', alignItems: "center", marginHorizontal: 10, justifyContent: 'space-between' }} >
          <Text style={{ fontSize: constants.FONT_12, fontWeight: '500', }} >{item.SafeZoneName}</Text>
          <TouchableOpacity onPress={() => Alert.alert(
            'PTC',
            'Do you want to remove this safe zone..',
            [
              {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
              { text: 'OK', onPress: () => this.deleteSafeZone(item) },
            ],
            { cancelable: false },
          )}
          >
            <Image style={{ height: 10, width: 10 }} source={require('../../assets/images/delete.png')} />
          </TouchableOpacity>
        </View>
        <View style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: 10, marginVertical: 5 }} >
          <Text style={{ fontSize: constants.FONT_12, }} >Created At : </Text>
          <Text style={{ fontSize: constants.FONT_12, }}>{item.RecordInsertDate}</Text>
        </View>
        <View style={{ flexDirection: 'row', marginHorizontal: 10, marginBottom: 5 }} >
          <Text style={{ fontSize: constants.FONT_12, }} >Address : </Text>
          <Text numberOfLines={2} style={{ fontSize: constants.FONT_12, flex: 1 }}>{item.address}</Text>
        </View>
      </View>
    )
  }

  render() {
    return (
      <View style={styles.MainContainer}>
        {
          (this.state.isLoading) ? <ActivityIndicator size="large" style={{ alignSelf: 'center' }} /> : (this.state.dataSource.length > 0) ?
            <FlatList
              style={{ marginTop: 10, }}
              data={this.state.dataSource}
              renderItem={({ item, index }) => this.renderItem(item, index)}
              extraData={this.state}
              keyExtractor={(item, index) => index.toString()}
            /> : <Image style={{ resizeMode: 'contain' }} source={require('../../assets/images/result_no.png')} />
        }
      </View>
    );
  }
}

export default SafeZoneList;