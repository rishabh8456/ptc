import {
  Platform,
  StyleSheet,
  Dimensions
} from "react-native";
import * as constants from '../../common/constant';

const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width

module.exports = StyleSheet.create({
  MainContainer: {
    flex: 1, backgroundColor: "rgb(228,228,215)", justifyContent: 'center', alignItems: 'center'
  },
  view1: {
    borderRadius: 5, marginBottom: 10, width: screenWidth - 20, backgroundColor: 'white', marginHorizontal: 10
  },
  view2: {
    flexDirection: 'row', justifyContent: 'space-between'
  },
  view3: {
    marginRight: 20, alignItems: 'center', justifyContent: 'center'
  },
  textStyle: {
    color: 'red', fontSize: constants.FONT_12, fontWeight: '500'
  },
  view4:{
    marginTop: 10, flexDirection: 'row'
  }
});
