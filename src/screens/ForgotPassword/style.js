import {
  Platform,
  StyleSheet,
  Dimensions
} from "react-native";
import * as constants from '../../common/constant';

const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width

module.exports = StyleSheet.create({
  MainView: {
    flex: 1, backgroundColor: 'rgb(218,219,202)', alignItems: 'center'
  },
  TextView: {
    alignItems: 'center', marginTop: screenHeight * 0.2
  },
  OTPText: {
    fontSize: constants.FONT_14, color: 'rgb(160,40,70)'
  },
  TextBox1: {
    marginTop: screenHeight * 0.05,
    borderWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 20,
    borderColor: 'black',
    height: screenHeight * 0.06,
    width: screenWidth - 50,
    backgroundColor: 'white',
    marginHorizontal: 15
  },
  TextBox2: {
    marginVertical: screenHeight * 0.015,
    borderWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 20,
    borderColor: 'black',
    height: screenHeight * 0.06,
    width: screenWidth - 50,
    backgroundColor: 'white',
    marginHorizontal: 15
  },
  TextBox3: {
    marginBottom: screenHeight * 0.05,
    borderWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 20,
    borderColor: 'black',
    height: screenHeight * 0.06,
    width: screenWidth - 50,
    backgroundColor: 'white',
    marginHorizontal: 15
  },
  TextInput: {
    textAlign: 'center', height: screenHeight * 0.05, flex: 1,
  },
  TouchView: {
    borderRadius: 30, height: screenHeight * 0.06, width: screenWidth * 0.55, backgroundColor: 'orange', alignItems: 'center', justifyContent: 'center'
  },
  saveText: {
    color: 'white', fontSize: screenHeight * 0.0259, fontWeight: 'bold'
  }


});
