import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
  Alert
} from 'react-native'

import * as constants from '../../common/constant';
const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width
import style from './style';

class ForgotPassword extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Forgot Password',
      headerTitleStyle: {
        color: 'white',
      },
      headerRight: (
        <View style={{ flex: 1 }} />
      ),
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.navigate('LogIn')} style={{ marginLeft: 10, marginBottom: 5 }
        }>
          <Image style={{ height: screenHeight * 0.03, width: screenWidth * 0.06 }} source={require('../../assets/images/reply.png')} />
        </TouchableOpacity >
      ),
      headerStyle: {
        backgroundColor: 'rgb(0,100,89)',
      },
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      otp: '',
      newPass: '',
      confirmPass: '',
      userData: this.props.navigation.state.params.otpData
    }
  }

  sendPassword() {

    const { userData } = this.state;
    const { otp, newPass, confirmPass } = this.state;
    if (otp === '') {
      alert("Please enter OTP")
    } else if (newPass === '') {
      alert('Please enter New Password')
    } else if (confirmPass === '') {
      alert('Please enter Confirm Password')
    } else {
      if (newPass.trim() !== confirmPass.trim()) {
        alert('Password do not match')
      } else {
        console.log(constants.MAINURL + constants.FORGOTPASSWORD + 'PTC/' + userData.UserID + '/' + userData.Mobile + '/' + otp + '/?NewPassword=' + this.state.newPass);

        fetch(constants.MAINURL + constants.FORGOTPASSWORD + 'PTC/' + userData.UserID + '/' + userData.Mobile + '/' + otp + '/?NewPassword=' + this.state.newPass)
          .then(response => response.json())
          .then((responseJson) => {
            console.log('Forget responseJson->', responseJson);
            if (responseJson.Status === 'Success') {
              Alert.alert(
                '',
                'Password Change Successfully',
                [
                  { text: 'OK', onPress: () => this.props.navigation.navigate('LogIn') },
                ],
                { cancelable: false },
              );
            } else {
              Alert.alert(responseJson.Status)
            }
          })
          .catch(error => console.log(error))
      }
    }
  }

  render() {
    return (
      <View style={style.MainView}>
        <View style={style.TextView} >
          <Text style={style.OTPText}>OTP valid for 10 minutes only...</Text>
        </View>
        <View style={style.TextBox1} >
          <TextInput
            keyboardType={'number-pad'}
            returnKeyType={'done'}
            placeholder={'Enter OTP'}
            value={this.state.otp}
            style={style.TextInput}
            onChangeText={(text) => this.setState({ otp: text })}
          />
        </View>
        <View style={style.TextBox2} >
          <TextInput
            secureTextEntry={true}
            placeholder={'New Password'}
            value={this.state.newPass}
            style={style.TextInput}
            onChangeText={(text) => this.setState({ newPass: text })}
          />
        </View>
        <View style={style.TextBox3} >
          <TextInput
            secureTextEntry={true}
            placeholder={'Confirm Password'}
            value={this.state.confirmPass}
            style={style.TextInput}
            onChangeText={(text) => this.setState({ confirmPass: text })}
          />
        </View>
        <TouchableOpacity onPress={() => this.sendPassword()} style={style.TouchView} >
          <Text style={style.saveText} >Save</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default ForgotPassword;