import React, { Component } from 'react';

import {
  View,
  Dimensions,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
  SafeAreaView,
  TextInput,
  AsyncStorage,
  Alert
} from 'react-native'
import styles from './style';
import * as constants from '../../common/constant';
const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width

class Registation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: '',
      userPassword: '',
      userPhone: '',
      userRef: '',
      isCheck: false
    }
  }


  userRegistration() {
    if (this.state.isCheck) {
      console.log(constants.MAINURL + constants.SIGNUP + 'PTC/PTC/' + this.state.userName + '/' + this.state.userPhone + '?' + constants.USERPASSWORD + this.state.userPassword);

      fetch(constants.MAINURL + constants.SIGNUP + 'PTC/PTC/' + this.state.userName + '/' + this.state.userPhone + '?' + constants.USERPASSWORD + this.state.userPassword)
        .then(response => response.json())
        .then((responseJson) => {
          console.log('registration Response--->', responseJson);
          if (responseJson.Status !== 'Unsuccessful') {
            AsyncStorage.setItem(constants.ISLOGIN, "true");
            AsyncStorage.setItem(constants.USERDATA, JSON.stringify(responseJson))
            this.props.navigation.navigate('HomeScreen', { data: responseJson })
          } else {
            Alert.alert('PTC', responseJson.RegistrationStatus)
          }

        })
        .catch(error => alert(error))
    } else {
      alert("please check terms and condition")
    }
  }

  render() {
    return (
      <SafeAreaView style={styles.MainContainer}>
        <ImageBackground style={{ flex: 1 }} source={require('../../assets/images/new_login_screen.png')} >
          <Image style={styles.logoImage} source={require('../../assets/images/ptclogo.png')} />
          <Image style={styles.logoText} source={require('../../assets/images/ptcnamelogo.png')} />
          <View style={styles.view1} >
            <View style={styles.view2} >
              <View style={styles.buttonView} >
                <Image style={styles.imageStyle} source={require('../../assets/images/manuser.png')} />
                <TextInput
                  placeholder={'Name'}
                  placeholderTextColor={'darkgreen'}
                  value={this.state.userName}
                  style={styles.textInputStyle}
                  onChangeText={(text) => this.setState({ userName: text })}
                />
              </View>
              <View style={[styles.buttonView, { marginTop: 10 }]} >
                <Image style={styles.imageStyle} source={require('../../assets/images/phonee.png')} />
                <TextInput
                  placeholder={'Phone'}
                  keyboardType={'number-pad'}
                  placeholderTextColor={'darkgreen'}
                  style={styles.textInputStyle}
                  value={this.state.userPhone}
                  maxLength={10}
                  onChangeText={(text) => this.setState({ userPhone: text })}
                />
              </View>
              <View style={[styles.buttonView, { marginTop: 10 }]} >
                <Image style={styles.imageStyle} source={require('../../assets/images/key.png')} />
                <TextInput
                  placeholder={'Password'}
                  placeholderTextColor={'darkgreen'}
                  style={styles.textInputStyle}
                  value={this.state.userPassword}
                  secureTextEntry={true}
                  maxLength={10}
                  onChangeText={(text) => this.setState({ userPassword: text })}
                />
              </View>
              <View style={[styles.buttonView, { marginTop: 10 }]} >
                <Image style={[styles.imageStyle]} source={require('../../assets/images/share.png')} />
                <TextInput
                  placeholder={'Ref. code (optional)'}
                  placeholderTextColor={'darkgreen'}
                  style={styles.textInputStyle}
                  value={this.state.userRef}
                  maxLength={10}
                  onChangeText={(text) => this.setState({ userRef: text })}
                />
              </View>
              <View style={styles.view3} >
                <TouchableOpacity onPress={() => this.userRegistration()} style={styles.registrationTouch} >
                  <Text style={styles.registrationText} >Registration</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.backTouch} >
                  <Image style={{ height: 30, width: 30 }} source={require('../../assets/images/backkkk_new.png')} />
                </TouchableOpacity>
              </View>
              <View style={styles.view4} >
                <TouchableOpacity style={{ marginLeft: screenWidth * 0.05 }} onPress={() => this.setState({ isCheck: !this.state.isCheck })}>
                  {
                    (!this.state.isCheck) ?
                      <Image style={{ height: 30, width: 30 }} source={{ uri: 'https://img.icons8.com/ios/50/000000/unchecked-checkbox.png' }} />
                      :
                      <Image style={{ height: 30, width: 30 }} source={{ uri: 'https://img.icons8.com/metro/26/000000/checked-2.png' }} />
                  }
                </TouchableOpacity>
                <Text style={{ color: 'darkgreen', marginLeft: 8, flex: 1 }} >I have read and agree with the <Text onPress={() => this.props.navigation.navigate('TermsCondition')} style={{ color: 'orange' }}>terms and condition</Text></Text>
              </View>
            </View>
          </View>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}

export default Registation;

