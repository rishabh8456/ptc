import {
  Platform,
  StyleSheet,
  Dimensions
} from "react-native";
import * as constants from '../../common/constant';

const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width

module.exports = StyleSheet.create({
  MainContainer: {
    flex: 1,
    backgroundColor: 'white'
  },
  buttonView: {
    borderWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 20,
    borderColor: 'black',
    height: screenHeight * 0.05,
    width: screenWidth - 30,
    backgroundColor: 'white',
    marginHorizontal: 15
  },
  imageStyle: {
    height: screenHeight * 0.05,
    width: screenWidth * 0.06,
    marginLeft: screenWidth * 0.06,
    marginRight: screenWidth * 0.1,
    resizeMode: 'contain'
  },
  logoImage: {
    marginTop: screenHeight * 0.08, resizeMode: 'contain', height: screenHeight * 0.15, alignSelf: "center"
  },
  logoText: {
    marginTop: 10, resizeMode: 'contain', height: screenHeight * 0.05, alignSelf: "center"
  },
  view1: {
    backgroundColor: 'transparent', flex: 1
  },
  view2: {
    backgroundColor: 'transparent', marginTop: screenHeight * 0.08, width: screenWidth
  },
  textInputStyle: {
    textAlign: 'center', height: screenHeight * 0.05, flex: 1, marginRight: screenWidth * 0.1
  },
  view3: {
    alignItems: 'center', marginTop: screenHeight * 0.05
  },
  registrationTouch: {
    borderRadius: 30, height: screenHeight * 0.06, width: screenWidth * 0.55, backgroundColor: 'orange', alignItems: 'center', justifyContent: 'center'
  },
  registrationText: {
    color: 'white', fontSize: screenHeight * 0.0259, fontWeight: 'bold'
  },
  backTouch: {
    alignSelf: 'flex-end', marginTop: 10, marginRight: screenWidth * 0.15
  },
  view4: {
    alignItems: 'center', flexDirection: 'row', marginTop: screenHeight * 0.06,
  }
});
